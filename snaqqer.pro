#-------------------------------------------------
#
# Project created by QtCreator 2019-07-14T14:55:42
#
#-------------------------------------------------

QT       += core gui dbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = snaqqer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

QMAKE_CXXFLAGS += -pedantic -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default \
                  -Wundef -Wno-unused #-Werror -Wshadow

SOURCES += \
        dbus_types.cpp \
        dialog_createconfig.cpp \
        diffwindow/diffwindow.cpp \
        diffwindow/filetreemodel.cpp \
        main.cpp \
        mainwindow.cpp \
        oldversions/oldversionslistmodel.cpp \
        oldversions/oldversionwindow.cpp \
        properties/propertieswidget.cpp \
        properties/propertieswindow.cpp \
        shared/restore.cpp \
        snapperapi.cpp \
        treemodel/treemodel.cpp \
        treemodel/treeitem.cpp \
        util.cpp

HEADERS += \
        dbus_types.h \
        dialog_createconfig.h \
        diffwindow/diffwindow.h \
        diffwindow/filetreemodel.h \
        mainwindow.h \
        oldversions/oldversionslistmodel.h \
        oldversions/oldversionwindow.h \
        properties/propertieswidget.h \
        properties/propertieswindow.h \
        qBittorrent/base/utils/version.h \
        shared/shared.h \
        snapperapi.h \
        treemodel/treemodel.h \
        treemodel/treeitem.h \
        util.h

FORMS += \
        dialog_createconfig.ui \
        diffwindow/diffwindow.ui \
        mainwindow.ui \
        oldversions/oldversionwindow.ui \
        properties/propertieswidget.ui \
        properties/propertieswindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
