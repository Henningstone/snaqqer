#include "snapperapi.h"

#include <QObject>
#include <QProcess>
#include <QDebug>
#include <QDBusConnection>
#include <QDBusArgument>
#include <QtConcurrent/QtConcurrent>

#include <util/dbus_types.h>
#include <util/util.h>

#define SERVICE QStringLiteral("org.opensuse.Snapper")
#define OBJECT QStringLiteral("/org/opensuse/Snapper")
#define INTERFACE QStringLiteral("org.opensuse.Snapper")

namespace snapper
{

void SnapperApi::staticInit()
{
    snapper_dbus_types_init();
}

QList<XConfigInfo> SnapperApi::listConfigs()
{
    QDBusMessage reply = dbusCall(QStringLiteral("ListConfigs"));
    //qDebug() << "listConfigs:" << reply;

    QDBusArgument arg = reply.arguments()[0].value<QDBusArgument>();
    QList<XConfigInfo> configs;
    arg >> configs;

    return configs;
}

XConfigInfo SnapperApi::getConfig(const QString& configName)
{
    QDBusMessage reply = dbusCall(QStringLiteral("GetConfig"), QList<QVariant>() << configName);
    //qDebug() << "getConfig:" << reply;

    XConfigInfo config;
    QDBusArgument arg = reply.arguments()[0].value<QDBusArgument>();
    arg >> config;
    //qDebug() << config.config_name << config.subvolume << config.raw;

    return config;
}

void SnapperApi::setConfig(const XConfigInfo& config)
{
    QDBusArgument arg;
    arg << config.raw;
    QDBusMessage reply = dbusCall(QStringLiteral("SetConfig"), QList<QVariant>() << config.config_name << QVariant::fromValue(arg));
    qDebug() << "setConfig:" << reply;
}

void SnapperApi::createConfig(const QString &name, const QString &subvolume, const QString &fstype, const QString &templateName)
{
    QDBusMessage reply = dbusCall(QStringLiteral("CreateConfig"), QList<QVariant>() << name << subvolume << fstype << templateName);
    qDebug() << "createConfig:" << reply;
}

QList<XSnapshot> SnapperApi::listSnapshots(const QString &configName)
{
    QDBusMessage reply = dbusCall(QStringLiteral("ListSnapshots"), QList<QVariant>() << configName);
    //qDebug() << "listSnapshots:" << reply;

    QList<XSnapshot> snapshots;
    QDBusArgument arg = reply.arguments()[0].value<QDBusArgument>();
    arg >> snapshots;
    qDebug() << "got" << snapshots.size() << "snapshots for" << configName;

    return snapshots;
}

XSnapshot SnapperApi::getSnapshot(const QString &configName, unsigned int snapNum)
{
    QDBusMessage reply = dbusCall(QStringLiteral("GetSnapshot"), QList<QVariant>() << configName << snapNum);
    qDebug() << "getSnapshot:" << reply;

    XSnapshot snapshot;
    QDBusArgument arg = reply.arguments()[0].value<QDBusArgument>();
    arg >> snapshot;
    return snapshot;
}

void SnapperApi::setSnapshot(const QString &configName, unsigned int snapNum, const QString &description, const QString &cleanup, const QMap<QString, QString> &userdata)
{
    QDBusArgument arg;
    arg << userdata;
    QDBusMessage reply = dbusCall(QStringLiteral("SetSnapshot"), QList<QVariant>() << configName << snapNum << description << cleanup  << QVariant::fromValue(arg));
    qDebug() << "setSnapshot:" << reply;
}

unsigned int SnapperApi::createSingleSnapshot(const QString &configName, const QString &description, const QString &cleanup, const QMap<QString, QString> &userdata)
{
    QDBusArgument arg;
    arg << userdata;
    QDBusMessage reply = dbusCall(QStringLiteral("CreateSingleSnapshot"), QList<QVariant>() << configName << description << cleanup << QVariant::fromValue(arg));
    qDebug() << "createSingleSnapshot:" << reply;
    return reply.arguments()[0].value<unsigned int>();
}

unsigned int SnapperApi::createPreSnapshot(const QString &configName, const QString &description, const QString &cleanup, const QMap<QString, QString> &userdata)
{
    QDBusArgument arg;
    arg << userdata;
    QDBusMessage reply = dbusCall(QStringLiteral("CreatePreSnapshot"), QList<QVariant>() << configName << description << cleanup << QVariant::fromValue(arg));
    qDebug() << "createPreSnapshot:" << reply;
    return reply.arguments()[0].value<unsigned int>();
}

unsigned int SnapperApi::createPostSnapshot(const QString &configName, unsigned int preNumber, const QString &description, const QString &cleanup, const QMap<QString, QString> &userdata)
{
    QDBusArgument arg;
    arg << userdata;
    QDBusMessage reply = dbusCall(QStringLiteral("CreatePostSnapshot"), QList<QVariant>() << configName << preNumber << description << cleanup << QVariant::fromValue(arg));
    qDebug() << "createPostSnapshot:" << reply;
    return reply.arguments()[0].value<unsigned int>();
}

void SnapperApi::deleteSnapshots(const QString &configName, const QList<unsigned int>& snapNums)
{
    QDBusMessage reply = dbusCall(QStringLiteral("DeleteSnapshots"), QList<QVariant>() << configName << QVariant::fromValue(snapNums));
    qDebug() << "deleteSnapshot:" << reply;
}

QFuture<void> SnapperApi::diffSnapshots(const QString &configName, unsigned int snapNum1, unsigned int snapNum2, ComparisonDoneSignalReceiver *doneSignalReceiver)
{
    bool *pRunning = new bool(true);
    doneSignalReceiver->setComparisonRunningIndicator(pRunning);
    return QtConcurrent::run(SnapperApi::diffSnapshotsImpl, configName, snapNum1, snapNum2, doneSignalReceiver, pRunning);
}

void SnapperApi::diffSnapshotsImpl(const QString &configName, unsigned int snapNum1, unsigned int snapNum2, ComparisonDoneSignalReceiver *doneSignalReceiver, volatile bool *pRunning)
{
    QDBusMessage reply;

    // try this to see if there is a comparison available already
    bool success;
    try {
        reply = dbusCall(QStringLiteral("GetFiles"), QList<QVariant>() << configName << snapNum1 << snapNum2);
        success = true;
        qDebug() << "cached comparison available for" << configName << snapNum1 << snapNum2;
    } catch (DBusErrorException&) {
        success = false;
    }

    if(!success)
    {
        // create comparison
        reply = dbusCall(QStringLiteral("CreateComparison"), QList<QVariant>() << configName << snapNum1 << snapNum2);
        qDebug() << "diffSnapshots-CreateComparison:" << reply;

        if(!*pRunning)
        {
            delete pRunning;
            return;
        }

        // get files
        reply = dbusCall(QStringLiteral("GetFiles"), QList<QVariant>() << configName << snapNum1 << snapNum2);
    }

    //unsigned int numFiles = reply.arguments()[0].value<unsigned int>();

    qDebug() << "diffSnapshots-GetFiles: [...]";// << reply;

    QList<XFile> files;
    QDBusArgument arg = reply.arguments()[0].value<QDBusArgument>();
    arg >> files;

    if(!*pRunning)
    {
        delete pRunning;
        return;
    }

    // emit done signal
    doneSignalReceiver->comparisonDone(files);
    delete pRunning;


    // delete comparison (do we *need* this? Keeping it out for now to speed up successive comparisons)
    //reply = dbusCall("DeleteComparison", QList<QVariant>() << configName << snapNum1 << snapNum2);
    //qDebug() << "diffSnapshots-DeleteComparison" << reply;
}

QFuture<void> SnapperApi::diffDirectories(const QString& subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString& snaprelFilePath, class ComparisonDoneSignalReceiver *doneSignalReceiver)
{
    bool *pRunning = new bool(true);
    doneSignalReceiver->setComparisonRunningIndicator(pRunning);
    void** moreArgs = new void*[2] { doneSignalReceiver, pRunning };
    return QtConcurrent::run(SnapperApi::diffDirectoriesImpl, subvolume, snapNum1, snapNum2, snaprelFilePath, moreArgs);
}

void SnapperApi::diffDirectoriesImpl(const QString& subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString& snaprelFilePath, void **moreArgs)
{
    ComparisonDoneSignalReceiver *doneSignalReceiver = static_cast<ComparisonDoneSignalReceiver*>(moreArgs[0]);
    volatile bool *pRunning = static_cast<bool*>(moreArgs[1]);

    QList<XFile> files;
    diffDirectoriesRecurse(subvolume, snapNum1, snapNum2, snaprelFilePath, &files, doneSignalReceiver, pRunning);
    std::sort(files.begin(), files.end(), [](const XFile& a, const XFile& b) -> bool { return a.name < b.name; });

    doneSignalReceiver->comparisonDone(files);
    delete pRunning;
    delete moreArgs;
}

void SnapperApi::diffDirectoriesRecurse(const QString &subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString &snaprelDirPath,
                                        QList<snapper::XFile> *plFiles, ComparisonDoneSignalReceiver *doneSignalReceiver, volatile bool *pRunning)
{
    if(!*pRunning)
        return;

    qDebug() << "diffDirectoriesRecurse" << snaprelDirPath;

    // iterate over all files in both directories and compare
    QStringList dirList = QDir(util::snapshotPath(subvolume, snapNum1, snaprelDirPath))
            .entryList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::DirsLast);
    dirList.append(
                QDir(util::snapshotPath(subvolume, snapNum2, snaprelDirPath))
                .entryList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::DirsLast)
                );

    qDebug() << "    -" << snapNum1 << snapNum2 << plFiles->size();
    qDebug() << "    +" << dirList.size() << util::snapshotPath(subvolume, snapNum1, snaprelDirPath);

    for(const QString& name : dirList)
    {
        if(!*pRunning)
            return;

        QFileInfo file1(util::snapshotPath(subvolume, snapNum1, snaprelDirPath + QStringLiteral("/") + name));
        QFileInfo file2(util::snapshotPath(subvolume, snapNum2, snaprelDirPath + QStringLiteral("/") + name));
        XFile diffInfo;
        diffInfo.name = QStringLiteral("/") + util::snapshotPathRelativify(subvolume, snapNum1, file1.absoluteFilePath());
        diffInfo.status = 0;
        qDebug() << "    >> " << name << diffInfo.name << file1.exists() << file2.exists();

        if (file1.exists() && !file2.exists())
            diffInfo.status |= FileStatusFlags::DELETED;
        else if (file2.exists() && !file1.exists())
            diffInfo.status |= FileStatusFlags::CREATED;
        else {
            // at this point we know that the file exists in both snapshots
            if (file1.isDir() != file2.isDir())
                diffInfo.status |= FileStatusFlags::_TYPE;

            if (file1.fileTime(QFile::FileTime::FileModificationTime) !=
                file2.fileTime(QFile::FileTime::FileModificationTime)
                || file1.fileTime(QFile::FileTime::FileBirthTime) != file2.fileTime(QFile::FileTime::FileBirthTime)
                || file1.size() != file2.size())
                diffInfo.status |= FileStatusFlags::_CONTENT;

            if (file1.permissions() != file2.permissions())
                diffInfo.status |= FileStatusFlags::PERMISSIONS;

            if (file1.ownerId() != file2.ownerId())
                diffInfo.status |= FileStatusFlags::OWNER;

            if (file1.groupId() != file2.groupId())
                diffInfo.status |= FileStatusFlags::GROUP;

            // TODO: technical debt: ACL and XATTR are missing here
        }

        // only track changes
        if(diffInfo.status != 0)
            plFiles->push_back(diffInfo);

        // recurse into subdirectories that exist in both snapshots
        if(file1.isDir() && file2.isDir())
            diffDirectoriesRecurse(
                    subvolume, snapNum1, snapNum2,
                    util::snapshotPathRelativify(subvolume, snapNum1, file1.absoluteFilePath()),
                    plFiles, doneSignalReceiver, pRunning
                    );
    }

}

bool SnapperApi::connect(const QString &name, QObject *receiver, const char *slot)
{
    return QDBusConnection::systemBus().connect(SERVICE, OBJECT, INTERFACE, name, receiver, slot);
}

bool SnapperApi::connect(const QString &name, const QString &signature, QObject *receiver, const char *slot)
{
    return QDBusConnection::systemBus().connect(SERVICE, OBJECT, INTERFACE, name, signature, receiver, slot);
}

bool SnapperApi::connect(const QString &name, const QStringList &argumentMatch, const QString &signature, QObject *receiver, const char *slot)
{
    return QDBusConnection::systemBus().connect(SERVICE, OBJECT, INTERFACE, name, argumentMatch, signature, receiver, slot);
}

QDBusMessage SnapperApi::dbusCall(const QString &method, const QList<QVariant>& args)
{
    QDBusMessage message = QDBusMessage::createMethodCall(SERVICE, OBJECT, INTERFACE, method);
    message.setAutoStartService(true);
    if(!args.empty())
        message.setArguments(args);

    // fire it off and check for errors
    QDBusMessage reply = QDBusConnection::systemBus().call(message);
    if(reply.type() == QDBusMessage::ErrorMessage)
    {
        qDebug() << "dbusCall exception from method: " << method << ", args: " << args;
        throw DBusErrorException(method, reply);
    }

    return reply;
}

const char *DBusErrorException::what() const noexcept
{
    std::string *err = new std::string(m_ErrorMessage.toStdString());
    return err->c_str();
}

const QString DBusErrorException::message() const
{
    if(m_Reply.errorName() == QStringLiteral("error.no_permissions"))
        return QStringLiteral("Permission denied.");
    else if(m_Reply.errorName() == QStringLiteral("error.unknown_config"))
        return QStringLiteral("Unknown snapper configuration");
    else if(m_Reply.errorName() == QStringLiteral("error.config_in_use"))
        return QStringLiteral("The configuration is currently in use by another process.");
    else
        return QStringLiteral("Unknown error: %1 (%2)").arg(m_Reply.errorName(), m_Reply.errorMessage());
}


ComparisonDoneSignalReceiver::~ComparisonDoneSignalReceiver()
{
    qDebug() << "ComparisonDoneSignalReceiver dtor";
}

}
