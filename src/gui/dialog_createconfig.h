#ifndef DIALOG_CREATECONFIG_H
#define DIALOG_CREATECONFIG_H

#include <QDialog>

namespace Ui {
class DialogCreateConfig;
}

class DialogCreateConfig : public QDialog
{
    Q_OBJECT

public:
    explicit DialogCreateConfig(QWidget *parent = nullptr);
    ~DialogCreateConfig();

private Q_SLOTS:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_toolButton_subvolume_clicked();

private:
    Ui::DialogCreateConfig *ui;
};

#endif // DIALOG_CREATECONFIG_H
