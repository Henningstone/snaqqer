#ifndef FILETREEMODEL_H
#define FILETREEMODEL_H

#include <QAbstractItemModel>
#include <QFileIconProvider>
#include <QFileInfo>
#include "snapperapi.h"

class FileTreeRow
{
public:
    const QFileInfo file;
    QString relPath;
    int created, modified, deleted;

    FileTreeRow() : m_parent(nullptr), m_myRowNumber(-1), m_status(0) {
        created = 0;
        modified = 0;
        deleted = 0;
    }

    FileTreeRow(const QString& prefix, const QString& path, FileTreeRow *parent, int myRowNumber, unsigned int status = 0)
        : file(prefix + path),
          relPath(path),
          m_parent(parent),
          m_myRowNumber(myRowNumber),
          m_status(status) {
        created = 0;
        modified = 0;
        deleted = 0;
    }

    ~FileTreeRow()
    {
        qDeleteAll(m_children);
    }

    /**
     * @brief adds a new child if it doesn't exist and decends down the tree
     * @param prefix the snapshot mountpoint
     * @param infix the path leading from the prefix to this particular file
     * @param name filename
     * @param status enum snapper::FileStatus
     * @return the newly added or found child
     */
    FileTreeRow *addChild(const QString& prefix, const QString& infix, const QString& name, unsigned int status = 0)
    {
        FileTreeRow *newChild;

        FileTreeRow *foundChild = findChild(name);
        if(foundChild)
            newChild = foundChild;
        else
        {
            newChild = new FileTreeRow(prefix, infix + QStringLiteral("/") + name, this, m_children.length(), status);
            m_children.append(newChild);
        }
        m_status |= status;
        return newChild;
    }

    void stealAllChildren(FileTreeRow *parent)
    {
        while(!parent->m_children.isEmpty())
        {
            FileTreeRow *ch = parent->m_children.takeFirst();
            this->m_children.append(ch);
            ch->m_parent = this;
        }
    }

    FileTreeRow *parent() { return m_parent; }

    FileTreeRow* child(int row)
    {
        if(row < 0 || row >= m_children.size())
            return nullptr;
        return m_children.at(row);
    }

    FileTreeRow *findChild(const QString& name)
    {
        for(FileTreeRow *row : m_children)
            if(row->file.fileName() == name)
                return row;
        return nullptr;
    }

    int myRowNumber() const { return m_myRowNumber; }

    unsigned int status() const { return m_status; }

    bool isCreated() const { return (m_status & 0x0F) == snapper::CREATED; }
    bool isModified() const { return (m_status & 0x0F) == snapper::CHANGED; }
    bool isDeleted() const { return (m_status & 0x0F) == snapper::DELETED; }

    int childCount() const { return m_children.length(); }

private:
    FileTreeRow *m_parent;
    QVector<FileTreeRow*> m_children;
    int m_myRowNumber;

    unsigned int m_status; // snapper::FileStatus
};

class FileTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    FileTreeModel(const QString &subvolume, unsigned int snapNum1, unsigned int snapNum2, QObject *parent = nullptr);
    virtual ~FileTreeModel() override;

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    FileTreeRow *getItem(const QModelIndex& index) const;

    void setupModelData(const QList<snapper::XFile> &files);

Q_SIGNALS:
    void setupProgress(int percent);
    void setupDone(FileTreeModel *model, int created, int modified, int deleted);

private Q_SLOTS:
    void onSetupDone(FileTreeRow *fakeroot, int created, int modified, int deleted);

private:
    FileTreeRow *rootItem;

    QString subvolume;
    unsigned int snapNum1, snapNum2;

    QFileIconProvider m_icons;

    std::atomic_bool m_alive;
    QFuture<void> m_thread;
    static void setupThread(FileTreeModel *model, QList<snapper::XFile> files);
};

Q_DECLARE_METATYPE(FileTreeRow*)


#endif // FILETREEMODEL_H
