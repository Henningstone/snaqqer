#ifndef PROPERTIESWINDOW_H
#define PROPERTIESWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QAbstractButton>

#include <util/dbus_types.h>

namespace Ui {
class PropertiesWindow;
}

class PropertiesWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PropertiesWindow(QWidget *parent = nullptr);
    ~PropertiesWindow() override;

    void selectTab(int index);

private Q_SLOTS:
    void onConfigCreated(const QString& configName);
    void onConfigModified(const QString& configName);
    void onConfigDeleted(const QString& configName);

    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::PropertiesWindow *ui;

    struct SnapperConfig
    {
        snapper::XConfigInfo config;
        bool editable;
    };

    QStringList snapperConfigNames;
    QList<SnapperConfig> snapperConfigs;

    bool saveAll();

protected:
    void closeEvent(QCloseEvent *event) override;
};

#endif // PROPERTIESWINDOW_H
