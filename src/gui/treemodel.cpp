﻿/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "treemodel.h"
#include "treeitem.h"
#include <util/util.h>

#include <QtWidgets>


TreeModel::TreeModel(const TreeRowData &headers, const QList<snapper::XSnapshot> &data, QObject *parent)
    : QAbstractItemModel(parent)
{
    rootItem = new TreeRow(headers);
    setupModelData(data, rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return TreeRow::NUM_COLS;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    TreeRow *item = getItem(index);
    return item->columnData(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    if(index.column() == 3 || index.column() == 4)
        return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    else
        return QAbstractItemModel::flags(index);
}

TreeRow *TreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TreeRow *item = static_cast<TreeRow*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}

QModelIndex TreeModel::getIndexBySnapshotNum(unsigned int snapNum, const QModelIndex &parent)
{
    // root is our parent through which we search
    TreeRow *root = getItem(parent);

    // go through all the parent's children to find a match
    for(int row = 0; row < root->childCount(); row++)
    {
        if(root->child(row)->columnData(TreeRow::COL_NUM).value<unsigned int>() == snapNum)
        {
            // found it
            return index(row, 0, parent);
        }
        else if(root->child(row)->childCount() > 0)
        {
            // this child has sub-children, search through them aswell
            QModelIndex thisChildsIndex = index(row, 0, parent);
            QModelIndex resultIndex = getIndexBySnapshotNum(snapNum, thisChildsIndex);
            if(resultIndex.isValid())
            {
                // found it in the sub-children
                return resultIndex;
            }
        }
    }

    // nothing found
    return QModelIndex();
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->columnData(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeRow *parentItem = getItem(parent);
    if (!parentItem)
        return QModelIndex();

    TreeRow *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    return QModelIndex();
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{    
    TreeRow *parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginInsertRows(parent, position, position + rows - 1);
    const bool success = parentItem->insertChildren(position, QVector<TreeRowData>(rows));
    endInsertRows();

    return success;
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeRow *childItem = getItem(index);
    TreeRow *parentItem = childItem ? childItem->parent() : nullptr;

    if (parentItem == rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->myRowNumber(), 0, parentItem);
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    TreeRow *parentItem = getItem(parent);
    if (!parentItem)
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    bool success = true;
    for(int i = 0; i < rows; i++)
    {
        bool removed = parentItem->removeChildRow(position + i);
        success &= removed;
    }

    endRemoveRows();

    return success;
}

void TreeModel::snapshotCreate(const snapper::XSnapshot &snapshot)
{
    QModelIndex index;
    TreeRow *parent = rootItem;
    if(snapshot.type == snapper::SnapshotType::POST)
    {
        // get pre-snap for this post-snap
        index = getIndexBySnapshotNum(snapshot.getPreNum());
        parent = getItem(index);
    }

    // append this row to the desired parent
    insertRows(parent->childCount(), 1, index);

    // write the data
    parent->lastChild()->readSnapshot(snapshot);
}

void TreeModel::snapshotModify(const snapper::XSnapshot &snapshot)
{
    const QModelIndex& rowIndex = getIndexBySnapshotNum(snapshot.getNum());
    TreeRow *node = getItem(rowIndex);

    // write the data
    node->readSnapshot(snapshot);

    qDebug() << rowIndex.parent() << "snapshotModify" << rowIndex;
    Q_EMIT dataChanged(rowIndex, index(rowIndex.row(), TreeRow::NUM_COLS-1, rowIndex.parent()));
}

void TreeModel::snapshotDelete(unsigned int snapNum)
{
    const QModelIndex& toRemoveIndex = getIndexBySnapshotNum(snapNum);

    // removeRow calls RemoveRows which calls removeChildRow on the TreeRow item
    // behind toRemoveIndex which attaches its children to its parent
    removeRow(toRemoveIndex.row());
}

const TreeRowData &TreeModel::rowData(const QModelIndex &rowIndex)
{
    TreeRow *row = getItem(index(rowIndex.row(), 0, rowIndex.parent()));
    return row->data();
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    const TreeRow *parentItem = getItem(parent);

    return parentItem ? parentItem->childCount() : 0;
}

bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

    TreeRow *item = getItem(index);

    Q_EMIT fieldEdited(index, value);

    bool columnChanged;
    if(index.column() == TreeRow::COL_CLEANUP)
    {
        // cleanup column; check input for validity and autocomplete prefixes to possible values
        QString newVal = util::smartCompleteCleanup(value.value<QString>().toLower());
        if(!util::isValidCleanup(newVal))
            return false;
        columnChanged = item->setColumnData(index.column(), newVal);
    }
    else
        columnChanged = item->setColumnData(index.column(), value);

    if (columnChanged)
    {
        Q_EMIT dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
        Q_EMIT rowChanged(index);
    }

    return columnChanged;
}

bool TreeModel::setHeaderData(int section, Qt::Orientation orientation,
                              const QVariant &value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    const bool result = rootItem->setColumnData(section, value);

    if (result)
        Q_EMIT headerDataChanged(orientation, section, section);

    return result;
}

void TreeModel::setupModelData(const QList<snapper::XSnapshot> &snapshots, TreeRow *topLevel)
{
    QMap<unsigned int, TreeRow*> preSnaps;

    for(const snapper::XSnapshot& snap : snapshots)
    {
        // find pre-snap if this is a post snap, otherwise add to top-level
        TreeRow *parent = topLevel;
        if (snap.getType() == snapper::SnapshotType::POST)
        {
            // yes, there can be post snaps without a pre-snap
            // it happens when you manually delete a pre-snap that
            // has a post-snap attached to it. In this case we're just
            // gonna insert it into the top level as if it was a single
            if(preSnaps.contains(snap.getPreNum()))
                parent = preSnaps[snap.getPreNum()];
        }

        // append this row to the desired parent
        parent->insertChildren(parent->childCount(), QVector<TreeRowData>() << TreeRowData(snap));

        // if this is a pre-snap, remember it in our list
        if(snap.getType() == snapper::SnapshotType::PRE)
            preSnaps[snap.getNum()] = topLevel->lastChild();
    }
}
