#ifndef DIFFWINDOW_H
#define DIFFWINDOW_H

#include <QMainWindow>
#include <QList>
#include "snapperapi.h"

namespace Ui {
class DiffWindow;
}

class DiffWindow : public QMainWindow, public snapper::ComparisonDoneSignalReceiver
{
    Q_OBJECT

public:
    DiffWindow(const snapper::XConfigInfo &snapperConfig, unsigned int snapNum1, unsigned int snapNum2, QWidget *parent = nullptr, const QString * const comparisonRoot = nullptr);
    virtual ~DiffWindow() override;

private Q_SLOTS:
    void onComparisonDone(QList<snapper::XFile> files);
    void onSetupProgress(int numDone);
    void onSetupDone(class FileTreeModel *model, int created, int modified, int deleted);

    void on_pushButton_oldVersions_clicked();
    void on_pushButton_diffCurrent_clicked();
    void on_pushButton_diffNew_clicked();
    void on_pushButton_openOlder_clicked();
    void on_pushButton_openNewer_clicked();
    void on_pushButton_restoreOlder_clicked();
    void on_pushButton_restoreNewer_clicked();

private:
    Ui::DiffWindow *ui;

    snapper::XConfigInfo m_snapperConfig;
    unsigned int m_snapNum1, m_snapNum2;

    void updateActions();

    bool *m_pComparisonRunning;
    void comparisonDone(const QList<snapper::XFile> &files) override;
    void setComparisonRunningIndicator(bool *pRunning) override { m_pComparisonRunning = pRunning; }

    // helper
    void openDiff(unsigned int compareSnapNum);
};

#endif // DIFFWINDOW_H
