#include <QDebug>
#include <QMimeType>
#include <QMimeDatabase>

#include <util/shared.h>
#include <util/util.h>

#include "diffwindow.h"
#include "filetreemodel.h"
#include "oldversionwindow.h"
#include "ui_diffwindow.h"
#include "snapperapi.h"

using snapper::SnapperApi;


DiffWindow::DiffWindow(const snapper::XConfigInfo &snapperConfig, unsigned int snapNum1, unsigned int snapNum2, QWidget *parent, const QString * const comparisonRoot) :
    QMainWindow(parent),
    ui(new Ui::DiffWindow),
    m_snapperConfig(snapperConfig),
    m_snapNum1(snapNum1),
    m_snapNum2(snapNum2)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    if(snapNum2 == 0)
    {
        // there is nothing "newer" to restore if we are diffing against current
        ui->pushButton_restoreNewer->hide();
        ui->pushButton_restoreOlder->setText(tr("Restore"));
        ui->pushButton_diffNew->hide();
        ui->pushButton_diffCurrent->setText(tr("Show Differences"));
    }

    // colourise legend
    // TODO read the colours from the settings (once we have settings, that is)
    ui->label_legendCreated->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("green")));
    ui->label_legendModified->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("yellow")));
    ui->label_legendDeleted->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("red")));
    ui->label_legendCM->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("cyan")));
    ui->label_legendCD->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("rgb(120, 8, 217)")));
    ui->label_legendMD->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("rgb(255, 140, 0)")));
    ui->label_legendCMD->setStyleSheet(QStringLiteral("QLabel { color: %1; }").arg(QStringLiteral("white")));

    ui->widget_treeView->hide();

    ui->statusbar->showMessage(tr("Comparing %1 %2..%3 - examining files").arg(m_snapperConfig.subvolume, QString::number(snapNum1), QString::number(snapNum2)));

    show();

    if(comparisonRoot)
    {
        qDebug() << "new DiffWindow for comparison root:" << *comparisonRoot;
        SnapperApi::diffDirectories(m_snapperConfig.subvolume, snapNum1, snapNum2,
                util::snapshotPathRelativify(m_snapperConfig.subvolume, 0, *comparisonRoot), this);
    }
    else
        SnapperApi::diffSnapshots(m_snapperConfig.config_name, snapNum1, snapNum2, this);
}

DiffWindow::~DiffWindow()
{
    qDebug() << "DiffWindow dtor";
    if(m_pComparisonRunning)
        *m_pComparisonRunning = false;
    delete ui;
}

void DiffWindow::onComparisonDone(QList<snapper::XFile> files)
{
    qDebug() << "onComparisonDone" << files.length();

    ui->statusbar->showMessage(tr("Comparing %1 %2..%3 - got %4 files, building tree").arg(
                                   m_snapperConfig.subvolume,
                                   QString::number(m_snapNum1),
                                   QString::number(m_snapNum2),
                                   QString::number(files.length())
                                   ));

    ui->progressBar->setRange(0, files.length());

    // fill tree view with files
    FileTreeModel *model = new FileTreeModel(m_snapperConfig.subvolume, m_snapNum1, m_snapNum2, ui->treeView);
    connect(model, &FileTreeModel::setupProgress, this, &DiffWindow::onSetupProgress);
    connect(model, &FileTreeModel::setupDone, this, &DiffWindow::onSetupDone);
    model->setupModelData(files);
}

void DiffWindow::onSetupProgress(int numDone)
{
    ui->progressBar->setValue(numDone);
}

void DiffWindow::onSetupDone(FileTreeModel *model, int created, int modified, int deleted)
{
    disconnect(model, &FileTreeModel::setupDone, this, &DiffWindow::onSetupDone);
    disconnect(model, &FileTreeModel::setupProgress, this, &DiffWindow::onSetupProgress);

    ui->treeView->setModel(model);
    connect(ui->treeView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &DiffWindow::updateActions);

    ui->statusbar->showMessage(tr("Comparing %1..%2 - showing %3 files: %4/%5/%6 c/m/d").arg(
                                   QString::number(m_snapNum1),
                                   QString::number(m_snapNum2),
                                   QString::number(created + modified + deleted),
                                   QString::number(created), QString::number(modified), QString::number(deleted)
                                   ));

    ui->widget_pleaseWait->hide();
    ui->widget_treeView->show();
}

void DiffWindow::comparisonDone(const QList<snapper::XFile> &files)
{
    m_pComparisonRunning = nullptr;

    // redirect into main thread
    QMetaObject::invokeMethod(this, "onComparisonDone", Q_ARG(QList<snapper::XFile>, files));
}

void DiffWindow::updateActions()
{
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();
    const bool hasCurrent = currentIndex.isValid();

    ui->pushButton_oldVersions->setEnabled(hasCurrent);

    bool diffButtonsEnabled = false;
    bool hasOlder = false;
    bool hasNewer = false;
    if (hasCurrent)
    {
        ui->treeView->closePersistentEditor(currentIndex);

        // check if file is a modified text file and enable the "Open diff viewer" button if it is
        FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
        FileTreeRow *row = model->getItem(currentIndex);
        qDebug() << row->file.fileName() << "modified?" << row->isModified() << ui->treeView->selectionModel()->selectedRows();
        if(row->isModified())
        {
            const QFileInfo file = row->file;

            QMimeDatabase db;
            QMimeType type = db.mimeTypeForFile(file.filePath());
            bool isTextFile = type.inherits(QStringLiteral("text/plain"));

            qDebug() << file.fileName() << "is text file?" << isTextFile << ui->treeView->selectionModel()->selectedRows();

            diffButtonsEnabled = isTextFile;
        }
        hasNewer = row->status() & (snapper::CREATED | snapper::CHANGED);
        hasOlder = row->status() & (snapper::DELETED | snapper::CHANGED);
    }

    ui->pushButton_diffCurrent->setEnabled(diffButtonsEnabled);
    ui->pushButton_diffNew->setEnabled(diffButtonsEnabled);

    ui->pushButton_openOlder->setEnabled(hasOlder);
    ui->pushButton_openNewer->setEnabled(hasNewer);
    ui->pushButton_restoreOlder->setEnabled(hasOlder);
    ui->pushButton_restoreNewer->setEnabled(hasNewer);
}

void DiffWindow::on_pushButton_oldVersions_clicked()
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    QModelIndexList selected = ui->treeView->selectionModel()->selectedRows();
    for(const QModelIndex& index : selected)
    {
        FileTreeRow *row = model->getItem(index);
        OldversionWindow *w = new OldversionWindow(m_snapperConfig.subvolume + QStringLiteral("/") + row->relPath, this);
        w->show();
    }
}

void DiffWindow::openDiff(unsigned int compareSnapNum)
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();

    if(!currentIndex.isValid())
        return;

    const QString &relPath = model->getItem(currentIndex)->relPath;

    QString file1Path = util::snapshotPath(m_snapperConfig.subvolume, m_snapNum1, relPath);
    QString file2Path = util::snapshotPath(m_snapperConfig.subvolume, compareSnapNum, relPath);

    do { // retry-loop
        const QString diffProgram = QStringLiteral("meld"); // TODO settings
        QProcess proc;
        bool success = proc.startDetached(diffProgram, {file1Path, file2Path});
        if(!success)
        {
            bool retry = util::displayErrorMessage(this,
                                         tr("Failed to start diff viewer"),
                                         tr("The program '%1' could not be started. Please check your settings and make sure it is installed.").arg(diffProgram), true);
            if(retry)
                continue;
        }
        break;
    } while(true);
}

void DiffWindow::on_pushButton_diffCurrent_clicked()
{
    openDiff(0);
}

void DiffWindow::on_pushButton_diffNew_clicked()
{
    openDiff(m_snapNum2);
}

void DiffWindow::on_pushButton_restoreOlder_clicked()
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();

    if(!currentIndex.isValid())
        return;

    const QString &path = model->getItem(currentIndex)->relPath;
    snapper::XSnapshot snap = snapper::SnapperApi::getSnapshot(m_snapperConfig.config_name, m_snapNum1);

    restoreFileFromSnap(this, path, m_snapperConfig, m_snapNum1, snap.getDate());
}

void DiffWindow::on_pushButton_restoreNewer_clicked()
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();

    if(!currentIndex.isValid())
        return;

    const QString &path = model->getItem(currentIndex)->relPath;
    snapper::XSnapshot snap = snapper::SnapperApi::getSnapshot(m_snapperConfig.config_name, m_snapNum1);

    restoreFileFromSnap(this, path, m_snapperConfig, m_snapNum2, snap.getDate());
}

void DiffWindow::on_pushButton_openOlder_clicked()
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();

    if(!currentIndex.isValid())
        return;

    const QString &path = model->getItem(currentIndex)->relPath;
    qDebug() << "openOlder" << path;
    util::openPath(util::snapshotPath(m_snapperConfig.subvolume, m_snapNum1, path));
}

void DiffWindow::on_pushButton_openNewer_clicked()
{
    FileTreeModel *model = qobject_cast<FileTreeModel*>(ui->treeView->model());
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();

    if(!currentIndex.isValid())
        return;

    const QString &path = model->getItem(currentIndex)->relPath;
    qDebug() << "openNewer" << path;
    util::openPath(util::snapshotPath(m_snapperConfig.subvolume, m_snapNum2, path));
}
