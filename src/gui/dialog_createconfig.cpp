#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>

#include <util/util.h>

#include "dialog_createconfig.h"
#include "ui_dialog_createconfig.h"
#include "snapperapi.h"

using snapper::SnapperApi;


static void scanFolder(QStringList *pDst, QDir startDir, QString prefix = QStringLiteral(""))
{
    QFileInfoList entries = startDir.entryInfoList();
    for(const QFileInfo& entry : entries)
    {
        if(entry.fileName() == QStringLiteral(".") || entry.fileName() == QStringLiteral(".."))
            continue;

        if(entry.isFile())
            pDst->append(prefix + entry.fileName());
        else
            scanFolder(pDst, entry.absoluteFilePath(), entry.fileName() + QStringLiteral("/"));
    }

    pDst->sort();
}


DialogCreateConfig::DialogCreateConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogCreateConfig)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    // get templates
    QDir templatesDir(QStringLiteral("/etc/snapper/config-templates/"));
    QStringList templates;
    scanFolder(&templates, templatesDir);
    ui->comboBox_template->addItems(templates);

    // set default if present
    if(templates.contains(QStringLiteral("default")))
        ui->comboBox_template->setCurrentText(QStringLiteral("default"));
}

DialogCreateConfig::~DialogCreateConfig()
{
    delete ui;
}

void DialogCreateConfig::on_buttonBox_accepted()
{
    qDebug() << "accepted";
    try {
        // for btrfs, create the '.snapshots' subvolume inside the subvolume that's to be snapshotted
        if(ui->comboBox_filesystem->currentText() == QStringLiteral("btrfs"))
        {
            QByteArray processOutput;
            if(!util::btrfsSubvolumeCreate(ui->lineEdit_subvolume->text(), &processOutput))
            {
                bool retry = util::displayErrorMessage(this,
                                          QStringLiteral("Failed to create the configuration ") + ui->lineEdit_configname->text(),
                                          QStringLiteral("btrfs output: ") + QString::fromUtf8(processOutput), true);
                if(retry)
                {
                    on_buttonBox_accepted();
                }

                // keep the dialog open if there was an error
                return;
            }
        }

        // create the snapper configuration
        SnapperApi::createConfig(
                    ui->lineEdit_configname->text(),
                    ui->lineEdit_subvolume->text(),
                    ui->comboBox_filesystem->currentText(),
                    ui->comboBox_template->currentText()
        );

    } catch (snapper::DBusErrorException& e) {
        bool retry = util::displayErrorMessage(this,
                                  QStringLiteral("Failed to create the configuration ") + ui->lineEdit_configname->text(),
                                  e.message(), true);
        if(retry)
        {
            on_buttonBox_accepted();
        }

        // keep the dialog open if there was an error
        return;
    }

    // all successful, close it
    close();
}

void DialogCreateConfig::on_buttonBox_rejected()
{
    qDebug() << "rejected";
}

void DialogCreateConfig::on_toolButton_subvolume_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Select subvolume"), QStringLiteral("/"));
    ui->lineEdit_subvolume->setText(path);
}
