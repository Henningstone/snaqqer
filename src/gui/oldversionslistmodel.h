#ifndef OLDVERSIONSLISTMODEL_H
#define OLDVERSIONSLISTMODEL_H

#include <QAbstractListModel>
#include <QFileIconProvider>
#include <QFileInfo>

#include <util/dbus_types.h>

struct ListRowData
{
    QVariant num;
    QVariant date;
    QVariant desc;
    QVariant size;

    QFileInfo file;

    ListRowData() {}

    ListRowData(QVariant date, QVariant desc, QVariant size)
        : date(date), desc(desc), size(size) {}
};

class OldversionsListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum { COL_NUM, COL_DATE, COL_DESC, COL_SIZE, COL_MTIME, NUM_COLS };

    OldversionsListModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override { return NUM_COLS; }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void addFileInfo(const snapper::XSnapshot& snapshot, const QFileInfo& file);

private:
    QList<ListRowData> m_listRows;
    QFileIconProvider m_icons;
};

#endif // OLDVERSIONSLISTMODEL_H
