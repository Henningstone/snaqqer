#include "filetreemodel.h"
#include <util/util.h>
#include "propertieswidget.h"

#include <QApplication>
#include <QtConcurrent/QtConcurrent>

FileTreeModel::FileTreeModel(const QString& subvolume, unsigned int snapNum1, unsigned int snapNum2, QObject *parent)
    : QAbstractItemModel(parent),
      subvolume(subvolume),
      snapNum1(snapNum1),
      snapNum2(snapNum2)
{
    m_alive = true;
    rootItem = new FileTreeRow(); // rootItem is the header
    rootItem->relPath = subvolume;
}

FileTreeModel::~FileTreeModel()
{
    m_alive = false;
    qDebug() << "FileTreeModel dtor waiting for thread...";
    m_thread.waitForFinished();
    qDebug() << " > FileTreeModel thread finished";
    delete rootItem;
}

void FileTreeModel::setupModelData(const QList<snapper::XFile>& files)
{
    m_thread.waitForFinished();
    m_thread = QtConcurrent::run(setupThread, this, files);
}

void FileTreeModel::setupThread(FileTreeModel *self, QList<snapper::XFile> files)
{
    FileTreeRow *const fakeroot = new FileTreeRow();

    QString snap1prefix = util::snapshotPath(self->subvolume, self->snapNum1);
    QString snap2prefix = util::snapshotPath(self->subvolume, self->snapNum2);

    const int numFiles = files.length();
    int created = 0;
    int modified = 0;
    int deleted = 0;
    for(int f = 0; f < numFiles; f++)
    {
        if(!self->m_alive)
            return;

        if(f % 100 == 0)
            Q_EMIT self->setupProgress(f);

        snapper::XFile& file = files[f];
        Q_ASSERT(self->subvolume == QStringLiteral("/") || !file.name.startsWith(self->subvolume));

        // alter status for our purposes
        if(file.status & snapper::_TYPE)
        {
            file.status &= ~(unsigned)snapper::_TYPE;
            file.status |= snapper::CHANGED;
        }

        if((file.status & snapper::XATTRS) || (file.status & snapper::ACL))
        {
            // TODO add option to enable XATTRS/ACL
            qDebug() << "[info] file has XATTRS or ACL change:" << file.name;
            file.status &= ~(unsigned)(snapper::XATTRS | snapper::ACL);
        }

        if(file.status == 0)
            continue;


        QVector<QStringRef> parts = file.name.midRef(1).split(QStringLiteral("/"));

        // break the path up into subparts and put them in a tree
        FileTreeRow *prev = fakeroot;
        QString infix;
        for(int i = 0; i < parts.length(); i++)
        {
            const QString *prefix;
            if(file.status & snapper::DELETED)
                prefix = &snap1prefix; // for deleted files, take the old file
            else if(i < parts.length() - 1)
                prefix = &self->subvolume; // for directories leading to our file, use the current // TODO don't!
            else
                prefix = &snap2prefix; // use the newer

            prev = prev->addChild(*prefix, infix, parts[i].toString(), file.status);
            infix.append(QStringLiteral("/")).append(parts[i]);

            if(file.status & snapper::CREATED)
                prev->created++;
            else if((file.status & snapper::CHANGED) || (file.status & snapper::META))
                prev->modified++;
            else if(file.status & snapper::DELETED)
                prev->deleted++;
        }

        if(file.status & snapper::CREATED)
            created++;
        else if((file.status & snapper::CHANGED) || (file.status & snapper::META))
            modified++;
        else if(file.status & snapper::DELETED)
            deleted++;
        else
        {
            qDebug() << "file scan encountered weird status" << file.status;
            Q_ASSERT(false);
        }
    }

    if(!self->m_alive)
        return;

    qRegisterMetaType<FileTreeRow*>();
    QMetaObject::invokeMethod(self, "onSetupDone", Qt::AutoConnection,
                              Q_ARG(FileTreeRow*, fakeroot),
                              Q_ARG(int, created),
                              Q_ARG(int, modified),
                              Q_ARG(int, deleted)
                              );

    qDebug() << "setupThread done" << created << modified << deleted;
}

void FileTreeModel::onSetupDone(FileTreeRow *fakeroot, int created, int modified, int deleted)
{
    qDebug() << "onSetupDone" << created << modified << deleted;

    int numFiles = created + modified + deleted;

    beginInsertRows(QModelIndex(), rootItem->childCount(), rootItem->childCount() + numFiles-1);
    rootItem->stealAllChildren(fakeroot);
    endInsertRows();

    delete fakeroot;

    Q_EMIT setupDone(this, created, modified, deleted);
}

QVariant FileTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation != Qt::Orientation::Horizontal)
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        if(section == 0)
            return subvolume;
    }

    return QVariant();
}

QModelIndex FileTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    //if(!parent.isValid())
    //    return createIndex(0, 0, rootItem);

    if(column != 0)
        return QModelIndex();

    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    FileTreeRow *parentItem = getItem(parent);
    if(!parentItem)
        return QModelIndex();

    FileTreeRow *childItem = parentItem->child(row);
    if(childItem)
        return createIndex(row, column, childItem);
    return QModelIndex();
}

QModelIndex FileTreeModel::parent(const QModelIndex &index) const
{
    if(!index.isValid())
        return QModelIndex();

    FileTreeRow *childItem = getItem(index);
    FileTreeRow *parentItem = childItem ? childItem->parent() : nullptr;

    if (parentItem == rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->myRowNumber(), 0, parentItem);
}

int FileTreeModel::rowCount(const QModelIndex &parent) const
{
    const FileTreeRow *parentItem = getItem(parent);

    return parentItem ? parentItem->childCount() : 0;
}

int FileTreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

QVariant FileTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.column() != 0)
        return QVariant();

    FileTreeRow *item = getItem(index);
    if(role == Qt::DisplayRole)
    {
        return item->file.fileName();
    }
    else if(role == Qt::ToolTipRole)
    {
        QMimeType type = QMimeDatabase().mimeTypeForFile(item->file.absoluteFilePath());

        return QStringLiteral("<center><b>%1</b></center>"
                       "<hr>"
                       "Type: %2<br>"
                       "Size: %3<br>"
                       "Modified: %4"
                       "%5").arg(
                    item->file.fileName(),
                    type.comment(),
                    item->file.isDir()
                        ? QStringLiteral("%1 (%2)").arg(
                              tr("%1 items").arg(QString::number(QDir(item->file.absoluteFilePath()).count())),
                              tr("%1 shown").arg(QString::number(item->childCount()))
                              )
                        : QLocale().formattedDataSize(item->file.size()),
                    item->file.lastModified().toString(),
                    item->file.isDir() ? QStringLiteral("<br><br>"
                                                 "created: %1<br>"
                                                 "modified: %2<br>"
                                                 "deleted: %3").arg(
                                             QString::number(item->created),
                                             QString::number(item->modified),
                                             QString::number(item->deleted)) : QStringLiteral(" ")
                    );

    }
    else if(role == Qt::DecorationRole)
    {
        return m_icons.icon(item->file);
    }
    else if(role == Qt::ForegroundRole)
    {
        // TODO we'll want to have setting to configure these in the future
#define MAIN_IS(STATUS) (((getItem(index)->status() & 0x0F)) == (STATUS))
        // attention: possible bugs: do not use MAIN_IS() and HAS_ALL() with CHANGED and META!
#define HAS(STATUS) (getItem(index)->status() & (STATUS))
        if(MAIN_IS(snapper::CREATED))
            return QBrush(Qt::green);
        else if(MAIN_IS(snapper::CHANGED))
            return QBrush(Qt::yellow);
        else if(MAIN_IS(snapper::DELETED))
            return QBrush(Qt::red);
        else if(MAIN_IS(snapper::CREATED | snapper::CHANGED))
            return QBrush(Qt::cyan);
        else if(MAIN_IS(snapper::CREATED | snapper::DELETED))
            return QBrush(QColor(120, 8, 217)); // purple
        else if(MAIN_IS(snapper::DELETED | snapper::CHANGED))
            return QBrush(QColor(255, 140, 0)); // orange
        else if(MAIN_IS(snapper::CREATED | snapper::DELETED | snapper::CHANGED))
            return QBrush(Qt::white);
        else if(HAS(snapper::META))
            return QBrush(Qt::darkYellow);
        else if(HAS(snapper::XATTRS | snapper::ACL))
            0; // ignore for now TODO add option to enable XATTRS/ACL colour
        else
            qDebug() << "FileTreeModel::data weird file status" << getItem(index)->status();
#undef HAS
#undef MAIN_IS
    }

    return QVariant();
}

FileTreeRow *FileTreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        FileTreeRow *item = static_cast<FileTreeRow*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}
