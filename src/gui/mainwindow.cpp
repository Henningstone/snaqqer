﻿#include <QDebug>
#include <QAbstractButton>
#include <QAbstractItemModel>

#include <util/util.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "treemodel.h"
#include "treeitem.h"
#include "dialog_createconfig.h"
#include "propertieswindow.h"
#include "diffwindow.h"
#include "snapperapi.h"

using snapper::SnapperApi;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    propertiesWindow(this)
{
    ui->setupUi(this);
    tabBar = new QTabBar(ui->contentsWidget);
    connect(tabBar, &QTabBar::currentChanged, this, &MainWindow::onTabChanged);
    ui->contentsWidgetLayout->insertWidget(1, tabBar);

    // add a popup menu to the 'Create New' button
    {
        QMenu *menu = new QMenu(ui->toolButton_createNew);
        menu->addAction(ui->actionCreateSingleSnapshot);
        menu->addAction(ui->actionCreatePrePostSnapshot);
        ui->toolButton_createNew->setMenu(menu);
    }

    // connect to remote signals
    bool success = true;
    success &= SnapperApi::connect(QStringLiteral("ConfigCreated"), this, SLOT(onConfigCreated(QString)));
    success &= SnapperApi::connect(QStringLiteral("ConfigModified"), this, SLOT(onConfigModified(QString)));
    success &= SnapperApi::connect(QStringLiteral("ConfigDeleted"), this, SLOT(onConfigDeleted(QString)));
    success &= SnapperApi::connect(QStringLiteral("SnapshotCreated"), this, SLOT(onSnapshotCreated(QString, quint32)));
    success &= SnapperApi::connect(QStringLiteral("SnapshotModified"), this, SLOT(onSnapshotModified(QString, quint32)));
    success &= SnapperApi::connect(QStringLiteral("SnapshotsDeleted"), this, SLOT(onSnapshotsDeleted(QString, snapper::XArray<quint32>)));
    if(!success)
    {
        qDebug() << ">>>>> MainWindow";
        qDebug() << ">>>>> ERROR: failed to connect on or more signals! dynamic updating will not be available.";
        qDebug() << ">>>>>";
    }

    // load configs
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    for(const snapper::XConfigInfo& config : configs)
    {
        onConfigCreated(config.config_name);
    }

    // build snapshot table
    rebuildTreeModel();
    updateUi();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConfigCreated(const QString &configName)
{
    qDebug() << "MainWindow received config created:" << configName;

    // test if this config is accessible
    bool accessible = true;
    try {
        SnapperApi::listSnapshots(configName);
    } catch (snapper::DBusErrorException&) {
        accessible = false;
        qDebug() << "> inaccessible" << configName;
    }

    // request info
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    for(const snapper::XConfigInfo& config : configs)
    {
        if(config.config_name == configName)
        {
            // found the newly added config
            m_snapperConfigs.append(config);
            tabBar->addTab(configName);
            tabBar->setTabEnabled(m_snapperConfigs.length()-1, accessible);
            qDebug() << "> tab added";
            break;
        }
    }

    updateUi();
}

void MainWindow::onConfigModified(const QString &configName)
{
    qDebug() << "MainWindow received config modified:" << configName;

    // test if this config is accessible
    try {
        SnapperApi::listSnapshots(configName);
        tabBar->setTabEnabled(tabIndexFromConfigName(configName), true);
        qDebug() << "> accessible" << configName;
    } catch (snapper::DBusErrorException&) {
        tabBar->setTabEnabled(tabIndexFromConfigName(configName), false);
        qDebug() << "> inaccessible" << configName;
    }
}

void MainWindow::onConfigDeleted(const QString &configName)
{
    qDebug() << "MainWindow received config deleted:" << configName;
    int index = tabIndexFromConfigName(configName);
    tabBar->removeTab(index);
    m_snapperConfigs.removeAt(index);
    updateUi();
}

void MainWindow::onSnapshotCreated(const QString &configName, quint32 snapNum)
{
    qDebug() << "snapshotCreated:" << configName << snapNum;
    showStatusMsg(tr("Snapshot %1 created for %2").arg(QString::number(snapNum), configName));

    if(configName == activeConfigName())
    {
        // add snapshot to currently displayed list
        snapper::XSnapshot newSnap = SnapperApi::getSnapshot(configName, snapNum);
        qobject_cast<TreeModel*>(ui->treeView->model())->snapshotCreate(newSnap);

        // auto-resize all columns
        for (int column = 0; column < ui->treeView->model()->columnCount(); column++) {
            ui->treeView->resizeColumnToContents(column);
        }

        // expand last item
        ui->treeView->expand(ui->treeView->model()->index(ui->treeView->model()->rowCount()-1, 0));
        ui->treeView->scrollToBottom();
    }
}

void MainWindow::onSnapshotModified(const QString &configName, quint32 snapNum)
{
    qDebug() << "snapshotModified:" << configName << snapNum;
    showStatusMsg(tr("Snapshot %1 in %2 was modified").arg(QString::number(snapNum), configName));

    if(configName == activeConfigName())
    {
        // apply modifications
        snapper::XSnapshot snap = SnapperApi::getSnapshot(configName, snapNum);
        qobject_cast<TreeModel*>(ui->treeView->model())->snapshotModify(snap);
    }
}

void MainWindow::onSnapshotsDeleted(const QString &configName, snapper::XArray<quint32> snapNumsArg)
{
    const QList<quint32> &snapNums = snapNumsArg.data;

    qDebug() << "snapshotsDeleted:" << configName << snapNums;
    QString msg = tr("Snapshots deleted from %1").arg(configName) + QStringLiteral(": ");
    for(int i = 0; i < snapNums.size(); i++)
    {
        qobject_cast<TreeModel*>(ui->treeView->model())->snapshotDelete(snapNums[i]);

        // prepare status message
        msg.append(QString::number((snapNums[i])));
        if(i < snapNums.size() - 1)
            msg.append(QStringLiteral(", "));
    }
    showStatusMsg(msg);
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::onTabChanged(int index)
{
    qDebug() << "tabBar changed" << index;
    if(!tabBar->isTabEnabled(index) && index > 0)
        tabBar->setCurrentIndex(index-1);
    rebuildTreeModel();
}

void MainWindow::updateActions()
{
    const bool hasCurrent = ui->treeView->selectionModel()->currentIndex().isValid();

    ui->pushButton_openSnapshot->setEnabled(hasCurrent);
    ui->pushButton_showDiff->setEnabled(hasCurrent && ui->treeView->selectionModel()->selectedRows().length() >= 2);
    if(ui->pushButton_showDiff->isEnabled())
    {
        QModelIndexList selectedRows = ui->treeView->selectionModel()->selectedRows();
        unsigned int snapNum1 = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(selectedRows.first()).num.value<unsigned int>();
        unsigned int snapNum2 = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(selectedRows.last()).num.value<unsigned int>();
        ui->pushButton_showDiff->setText(tr("Compare %1..%2").arg(snapNum1).arg(snapNum2));
    }
    else
        ui->pushButton_showDiff->setText(tr("Show Differences"));


    bool DeleteEnabled = false;
    if (hasCurrent)
    {
        ui->treeView->closePersistentEditor(ui->treeView->selectionModel()->currentIndex());

        QModelIndexList selectedRows = ui->treeView->selectionModel()->selectedRows();
        if(selectedRows.length() > 1)
        {
            // when multiple rows are selected we can always delete *something* at least
            DeleteEnabled = true;
        }
        else if(selectedRows.length() > 0)
        {
            // can not delete the 'current' state
            TreeModel *model = qobject_cast<TreeModel*>(ui->treeView->model());
            DeleteEnabled = model->rowData(selectedRows.first()).num.value<unsigned int>() != 0;
        }
    }

    ui->pushButton_deleteSnapshot->setEnabled(DeleteEnabled);
}

void MainWindow::onFieldEdited(const QModelIndex &index, const QVariant &newValue)
{
    qDebug() << "onModelDataChanged" << index.column() << index.row() << index.data().value<QString>() << newValue;

    if(index.column() == TreeRow::COL_CLEANUP)
    {
        QString str = newValue.value<QString>();
        if(!util::isValidCleanup(util::smartCompleteCleanup(str)))
        {
            util::displayErrorMessage(this, tr("Invalid input") + QStringLiteral(": ") + str,
                                      tr("The field 'cleanup' must be one of 'timeline', 'number', 'empty-pre-post' or empty"), false);
            return;
        }
    }
}

void MainWindow::onRowChanged(const QModelIndex &index)
{
    const TreeRowData& row = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(index);
    qDebug() << "onRowChanged" << row.num;
    SnapperApi::setSnapshot(activeConfigName(),
            row.num.value<unsigned int>(),
            row.desc.value<QString>(), row.cleanup.value<QString>(),
            QMap<QString, QString>()); // TODO userdata
}

void MainWindow::rebuildTreeModel()
{
    TreeRowData headers(QStringLiteral("#"), tr("Date"), tr("User"), tr("Description"), tr("Cleanup"), tr("Size"));

    QList<snapper::XSnapshot> snapshots;
    if(m_snapperConfigs.length() > 0)
    {
        try {
            snapshots = SnapperApi::listSnapshots(activeConfigName());
        } catch (snapper::DBusErrorException &e) {
            util::displayErrorMessage(this, QStringLiteral("Error - Snaqqer"), QStringLiteral("rebuildTreeModel: ") + e.message(), false);
        }

        ui->statusBar->showMessage(activeConfigName() + QStringLiteral(": ") + QString::number(snapshots.length()) + QStringLiteral(" ") + tr("snapshots"));
    }
    else
        ui->statusBar->showMessage(tr("No configs. Use 'File > New Configuration' to create one."));

    TreeModel *model = new TreeModel(headers, snapshots);
    ui->treeView->setModel(model);

    updateActions();

    // auto-resize all columns
    for (int column = 0; column < model->columnCount(); column++) {
        ui->treeView->resizeColumnToContents(column);
    }

    // expand all items
    for(int row = 0; row < ui->treeView->model()->rowCount(); row++) {
        ui->treeView->expand(ui->treeView->model()->index(row, 0));
    }
    ui->treeView->scrollToBottom();

    // listen for selection
    connect(ui->treeView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &MainWindow::updateActions);
    connect(qobject_cast<TreeModel*>(ui->treeView->model()), &TreeModel::fieldEdited,
            this, &MainWindow::onFieldEdited);
    connect(qobject_cast<TreeModel*>(ui->treeView->model()), &TreeModel::rowChanged,
            this, &MainWindow::onRowChanged);
}

void MainWindow::updateUi()
{
    bool hasConfigs = !m_snapperConfigs.empty();
    ui->label_noConfigs->setVisible(!hasConfigs);
    ui->contentsWidget->setVisible(hasConfigs);
}

void MainWindow::showStatusMsg(const QString &message)
{
    statusBar()->showMessage(QStringLiteral("[%1] %2").arg(QTime::currentTime().toString(), message));
}

int MainWindow::tabIndexFromConfigName(const QString &configName) const
{
    for(int i = 0; i < m_snapperConfigs.length(); i++) {
        if(m_snapperConfigs[i].config_name == configName)
            return i;
    }
    return -1;
}

//
// actions
//

void MainWindow::on_actionNewConfiguration_triggered()
{
    // open create configuration dialog
    qDebug() << "new config";
    DialogCreateConfig *dialog = new DialogCreateConfig(this);
    dialog->show();
}

void MainWindow::on_treeView_expanded(const QModelIndex&)
{
    ui->treeView->resizeColumnToContents(0);
}

void MainWindow::on_toolButton_createNew_clicked()
{
    qDebug() << "new quick snapshot";
    qDebug() << SnapperApi::createSingleSnapshot(activeConfigName(), tr("quick snapshot"), QStringLiteral("number"));
}

void MainWindow::on_toolButton_createNew_triggered(QAction *arg1)
{
    qDebug() << "new custom snapshot";
}

void MainWindow::on_pushButton_openSnapshot_clicked()
{
    unsigned int snapNum = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(ui->treeView->currentIndex()).num.value<unsigned int>();
    qDebug() << "show contents" << snapNum;

    util::openPath(util::snapshotPath(activeConfigSubvolume(), snapNum));
}

void MainWindow::on_pushButton_showDiff_clicked()
{
    QModelIndexList selectedRows = ui->treeView->selectionModel()->selectedRows();
    //if(selectedRows.length() < 2)
      //  return;

    unsigned int snapNum1 = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(selectedRows.first()).num.value<unsigned int>();
    unsigned int snapNum2 = qobject_cast<TreeModel*>(ui->treeView->model())->rowData(selectedRows.last()).num.value<unsigned int>();

    new DiffWindow(m_snapperConfigs[activeTabIndex()], snapNum1, snapNum2, this);
}

void MainWindow::on_pushButton_deleteSnapshot_clicked()
{
    QModelIndexList selectedRows = ui->treeView->selectionModel()->selectedRows();
    if(selectedRows.empty())
        return;

    TreeModel *model = qobject_cast<TreeModel*>(ui->treeView->model());

    QString message;
    if(selectedRows.length() == 1)
         message = tr("Do you really want to delete snapshot %1 (%2) from config %3?").arg(
                     QString::number(model->rowData(selectedRows.first()).num.value<unsigned int>()),
                     model->rowData(selectedRows.first()).desc.value<QString>(),
                     activeConfigName());
    else
        message = tr("Do you really want to delete the selected snapshots?");

    bool doDelete = util::displayQuestionMessage(this, tr("Confirm snapshot deletion"), message);

    if(doDelete)
    {
        QList<unsigned int> snapNums;
        for(auto index : selectedRows)
        {
            unsigned int snapNum = model->rowData(index).num.value<unsigned int>();
            qDebug() << "delete: selected row " << index << " snapNum " << snapNum;
            if(snapNum != 0)
                snapNums.append(snapNum);
        }
        try
        {
            SnapperApi::deleteSnapshots(activeConfigName(), snapNums);
        } catch(snapper::DBusErrorException &e) {
            util::displayErrorMessage(this, tr("Failed to delete"), e.message(), false);
        }
    }
}

void MainWindow::on_pushButton_properties_clicked()
{
    qDebug() << "open properties";
    propertiesWindow.selectTab(activeTabIndex());
    propertiesWindow.show();
    propertiesWindow.activateWindow();
}
