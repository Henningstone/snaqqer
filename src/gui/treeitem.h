/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TREEITEM_H
#define TREEITEM_H

#include <QVariant>
#include <QVector>

#include <util/dbus_types.h>

struct TreeRowData
{
    QVariant num;
    QVariant date;
    QVariant user;
    QVariant desc;
    QVariant cleanup;
    QVariant size;

    TreeRowData() = default;
    TreeRowData(const QVariant& num, const QVariant& date, const QVariant& user, const QVariant& desc, const QVariant& cleanup, const QVariant& size)
        : num(num), date(date), user(user), desc(desc), cleanup(cleanup), size(size) {}

    TreeRowData(const snapper::XSnapshot &snapshot) { readSnapshot(snapshot); }
    void readSnapshot(const snapper::XSnapshot &snapshot);
};

//! [0]
class TreeRow
{
public:
    enum { COL_NUM, COL_DATE, COL_USER, COL_DESC, COL_CLEANUP, COL_SIZE, NUM_COLS };

    TreeRow(const TreeRowData& data, TreeRow *parent = nullptr);
    ~TreeRow();

    TreeRow *child(int row);
    TreeRow *lastChild() { return m_childRows.last(); }
    int childCount() const { return m_childRows.count(); }
    bool insertChildren(int position, const QVector<TreeRowData> &datas);
    TreeRow *parent() { return m_parentRow; }
    bool removeChildRow(int position);
    int myRowNumber() const;
    const TreeRowData& data() const { return m_columnData; }
    void readSnapshot(const snapper::XSnapshot &snapshot);

    bool setColumnData(int col, const QVariant& value);
    QVariant columnData(int col);

    // debug
    void dbgPrint(QString indent = QStringLiteral(""));

private:
    QVector<TreeRow*> m_childRows;
    TreeRowData m_columnData;
    TreeRow *m_parentRow;
};
//! [0]

#endif // TREEITEM_H
