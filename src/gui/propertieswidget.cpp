#include <QDebug>
#include <QPushButton>
#include <QDialogButtonBox>

#include <util/util.h>

#include "propertieswidget.h"
#include "ui_propertieswidget.h"
#include "snapperapi.h"

using snapper::SnapperApi;


PropertiesWidget::PropertiesWidget(const snapper::XConfigInfo& config, bool editable, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PropertiesWidget)
{
    ui->setupUi(this);
    ui->label_noPermission->setText(ui->label_noPermission->text().arg(config.config_name, util::currentUserName()));

    updatePropertiesUi(config, editable);
    show();
}

PropertiesWidget::~PropertiesWidget()
{
    qDebug() << "PropertiesWidget dtor " << m_config.config_name;
    delete ui;
}

void PropertiesWidget::updatePropertiesUi(const snapper::XConfigInfo& config, bool editable)
{
    m_config = config;

    ui->label_noPermission->setVisible(!editable);
    ui->mainFrame->setEnabled(editable);

    // read basic info
    ui->lineEdit_users->setText(config.raw[QStringLiteral("ALLOW_USERS")]);
    ui->lineEdit_groups->setText(config.raw[QStringLiteral("ALLOW_GROUPS")]);
    ui->lineEdit_subvolume->setText(config.raw[QStringLiteral("SUBVOLUME")]);
    ui->lineEdit_filesystem->setText(config.raw[QStringLiteral("FSTYPE")]);

    // read settings
    ui->checkBox_useTimeline->setChecked(config.raw[QStringLiteral("TIMELINE_CREATE")] == QStringLiteral("yes"));
    ui->checkBox_backgroundComparison->setChecked(config.raw[QStringLiteral("BACKGROUND_COMPARISON")] == QStringLiteral("yes"));
    ui->checkBox_syncAcl->setChecked(config.raw[QStringLiteral("SYNC_ACL")] == QStringLiteral("yes"));

    // read cleanup preferences
    ui->checkBox_enableTimelineCleanup->setChecked(config.raw[QStringLiteral("TIMELINE_CLEANUP")] == QStringLiteral("yes"));
    ui->checkBox_enableNumberCleanup->setChecked(config.raw[QStringLiteral("NUMBER_CLEANUP")] == QStringLiteral("yes"));
    ui->checkBox_enableEmptyCleanup->setChecked(config.raw[QStringLiteral("EMPTY_PRE_POST_CLEANUP")] == QStringLiteral("yes"));

    // read timeline cleanup
    ui->spinBox_timelineHourly->setValue(config.raw[QStringLiteral("TIMELINE_LIMIT_HOURLY")].toInt());
    ui->spinBox_timelineDaily->setValue(config.raw[QStringLiteral("TIMELINE_LIMIT_DAILY")].toInt());
    ui->spinBox_timelineWeekly->setValue(config.raw[QStringLiteral("TIMELINE_LIMIT_WEEKLY")].toInt());
    ui->spinBox_timelineMonthly->setValue(config.raw[QStringLiteral("TIMELINE_LIMIT_MONTHLY")].toInt());
    ui->spinBox_timelineYearly->setValue(config.raw[QStringLiteral("TIMELINE_LIMIT_YEARLY")].toInt());
    ui->spinBox_timelineMinAge->setValue(config.raw[QStringLiteral("TIMELINE_MIN_AGE")].toInt());

    // read number cleanup
    ui->spinBox_numberLimit->setValue(config.raw[QStringLiteral("NUMBER_LIMIT")].toInt());
    ui->spinBox_numberLimitImportant->setValue(config.raw[QStringLiteral("NUMBER_LIMIT_IMPORTANT")].toInt());
    ui->spinBox_numberMinAge->setValue(config.raw[QStringLiteral("NUMBER_MIN_AGE")].toInt());

    // read empty cleanup
    ui->spinBox_EmptyMinAge->setValue(config.raw[QStringLiteral("EMPTY_PRE_POST_MIN_AGE")].toInt());

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Save)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Discard)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::StandardButton::RestoreDefaults)->setEnabled(editable);
}

snapper::XConfigInfo PropertiesWidget::readPropertiesUi() const
{
    snapper::XConfigInfo config(m_config);

    // read basic info
    config.raw[QStringLiteral("ALLOW_USERS")] = ui->lineEdit_users->text();
    config.raw[QStringLiteral("ALLOW_GROUPS")] = ui->lineEdit_groups->text();
    config.raw[QStringLiteral("SUBVOLUME")] = ui->lineEdit_subvolume->text();
    config.raw[QStringLiteral("FSTYPE")] = ui->lineEdit_filesystem->text();

    // read settings
    config.raw[QStringLiteral("TIMELINE_CREATE")] = (ui->checkBox_useTimeline->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));
    config.raw[QStringLiteral("BACKGROUND_COMPARISON")] = (ui->checkBox_backgroundComparison->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));
    config.raw[QStringLiteral("SYNC_ACL")] = (ui->checkBox_syncAcl->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));

    // read cleanup preferences
    config.raw[QStringLiteral("TIMELINE_CLEANUP")] = (ui->checkBox_enableTimelineCleanup->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));
    config.raw[QStringLiteral("NUMBER_CLEANUP")] = (ui->checkBox_enableNumberCleanup->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));
    config.raw[QStringLiteral("EMPTY_PRE_POST_CLEANUP")] = (ui->checkBox_enableEmptyCleanup->isChecked() ? QStringLiteral("yes") : QStringLiteral("no"));

    // read timeline cleanup
    config.raw[QStringLiteral("TIMELINE_LIMIT_HOURLY")] = QString::number(ui->spinBox_timelineHourly->value());
    config.raw[QStringLiteral("TIMELINE_LIMIT_DAILY")] = QString::number(ui->spinBox_timelineDaily->value());
    config.raw[QStringLiteral("TIMELINE_LIMIT_WEEKLY")] = QString::number(ui->spinBox_timelineWeekly->value());
    config.raw[QStringLiteral("TIMELINE_LIMIT_MONTHLY")] = QString::number(ui->spinBox_timelineMonthly->value());
    config.raw[QStringLiteral("TIMELINE_LIMIT_YEARLY")] = QString::number(ui->spinBox_timelineYearly->value());
    config.raw[QStringLiteral("TIMELINE_MIN_AGE")] = QString::number(ui->spinBox_timelineMinAge->value());

    // read number cleanup
    config.raw[QStringLiteral("NUMBER_LIMIT")] = QString::number(ui->spinBox_numberLimit->value());
    config.raw[QStringLiteral("NUMBER_LIMIT_IMPORTANT")] = QString::number(ui->spinBox_numberLimitImportant->value());
    config.raw[QStringLiteral("NUMBER_MIN_AGE")] = QString::number(ui->spinBox_numberMinAge->value());

    // read empty cleanup
    config.raw[QStringLiteral("EMPTY_PRE_POST_MIN_AGE")] = QString::number(ui->spinBox_EmptyMinAge->value());

    // set other data
    config.config_name = m_config.config_name;
    config.subvolume = config.raw[QStringLiteral("SUBVOLUME")];

    return config;
}

bool PropertiesWidget::save()
{
    if(!hasChanges())
        return true;

    qDebug() << "--------------- save config" << m_config.config_name;

    snapper::XConfigInfo config = readPropertiesUi();
    config.raw.remove(QStringLiteral("SUBVOLUME"));
    config.raw.remove(QStringLiteral("FSTYPE"));
    qDebug() << config.config_name << config.raw;

    bool success;
    while(true) // retry-loop
    {
        try {
            SnapperApi::setConfig(config);
            success = true;
        } catch (snapper::DBusErrorException &e) {
            success = false;
            if(util::displayErrorMessage(this, QString(tr("Failed to save config")) + QStringLiteral(" ") + config.config_name, e.message(), true))
                continue;
        }
        break;
    }

     qDebug() << "------------ done saving" << m_config.config_name;

     return success;
}

void PropertiesWidget::discard()
{
    qDebug() << "discarding changed properties for" << m_config.config_name;
    updatePropertiesUi(m_config, true);
}

bool PropertiesWidget::hasChanges() const
{
    return readPropertiesUi() != m_config;
}

void PropertiesWidget::on_checkBox_enableTimelineCleanup_toggled(bool checked)
{
    ui->groupBox_cleanupTimeline->setEnabled(checked);
}

void PropertiesWidget::on_checkBox_enableNumberCleanup_toggled(bool checked)
{
    ui->groupBox_cleanupNumber->setEnabled(checked);
}

void PropertiesWidget::on_checkBox_enableEmptyCleanup_toggled(bool checked)
{
    ui->groupBox_cleanupEmpty->setEnabled(checked);
}

void PropertiesWidget::elementEdited()
{
    snapper::XConfigInfo changedCfg = readPropertiesUi();
    bool differences = changedCfg != m_config;

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Save)->setEnabled(differences);
    ui->buttonBox->button(QDialogButtonBox::StandardButton::Discard)->setEnabled(differences);
    //ui->buttonBox->button(QDialogButtonBox::StandardButton::RestoreDefaults)->setEnabled(changedCfg != s_DefaultConfig);
}

void PropertiesWidget::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->buttonRole(button)) {
    case QDialogButtonBox::AcceptRole:
    {
        save();
    } break;

    case QDialogButtonBox::DestructiveRole:
    {
        // discard
        if(util::displayQuestionMessage(this, tr("Confirm discard"), tr("Do you really wish to discard all your changes on this page?")))
            discard();
    } break;

    case QDialogButtonBox::ResetRole:
    {
        // defaults
        qDebug() << "setting defaults";
        // TODO implement
    } break;

    default: break;
    }
}
