#ifndef SNAPPERAPI_H
#define SNAPPERAPI_H

#include <memory>
#include <exception>
#include <QString>
#include <QList>
#include <QDateTime>
#include <QPointer>
#include <QProcess>
#include <QDBusMessage>
#include <QFuture>

#include <util/dbus_types.h>


namespace snapper {

struct Snapshot
{
    int id;
    enum EType { SINGLE, PRE, POST } type;
    std::shared_ptr<Snapshot> parent;
    QDateTime date;
    QString user;
    enum ECleanup { NONE, NUMBER, TIMELINE, EMPTY_PRE_POST } cleanup;
    QString description;
};


class DBusErrorException : public std::exception
{
    const QString m_ErrorMessage;
    const QDBusMessage m_Reply;
public:
    DBusErrorException(const QString& dbusMethod, const QDBusMessage& reply)
        : m_ErrorMessage(QStringLiteral("DBusErrorException: when calling dbus method '%1': %2: %3").arg(dbusMethod, reply.errorName(), reply.errorMessage())),
          m_Reply(reply)
    {
    }

    const char* what() const noexcept override;

    const QString message() const;
    const QDBusMessage& reply() const { return m_Reply; }
};


class SnapperApi// : public QObject
{
    //Q_OBJECT

public:
    // must be called at application startup to initialise metatypes
    static void staticInit();

    // DBus calls. All of these functions can throw DBusErrorException if an error occurs.
    static QList<XConfigInfo> listConfigs();
    static XConfigInfo getConfig(const QString& configName);
    static void setConfig(const XConfigInfo& config);
    static void createConfig(const QString& name, const QString& subvolume, const QString& fstype, const QString& templateName);
    static QList<XSnapshot> listSnapshots(const QString& configName);
    static XSnapshot getSnapshot(const QString& configName, unsigned int snapNum);
    static void setSnapshot(const QString& configName, unsigned int snapNum, const QString& description, const QString& cleanup, const QMap<QString, QString>& userdata);
    static unsigned int createSingleSnapshot(const QString& configName, const QString& description, const QString& cleanup, const QMap<QString, QString>& userdata = QMap<QString, QString>());
    static unsigned int createPreSnapshot(const QString& configName, const QString& description, const QString& cleanup, const QMap<QString, QString>& userdata = QMap<QString, QString>());
    static unsigned int createPostSnapshot(const QString& configName, unsigned int preNumber, const QString& description, const QString& cleanup, const QMap<QString, QString>& userdata = QMap<QString, QString>());
    static void deleteSnapshots(const QString& configName, const QList<unsigned int> &snapNums);
    static QFuture<void> diffSnapshots(const QString& configName, unsigned int snapNum1, unsigned int snapNum2, class ComparisonDoneSignalReceiver *doneSignalReceiver);
    static QFuture<void> diffDirectories(const QString &subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString &snaprelFilePath, class ComparisonDoneSignalReceiver *doneSignalReceiver);


    // methods to connect to remote signals
    static bool connect(const QString &name, QObject *receiver, const char *slot);
    static bool connect(const QString &name, const QString& signature, QObject *receiver, const char *slot);
    static bool connect(const QString &name, const QStringList &argumentMatch, const QString& signature, QObject *receiver, const char *slot);

private:
    SnapperApi(){}

    /**
     * @brief call a remote function of snapper over the DBus
     * @param method method to call
     * @param args arguments to send with the call
     * @throws DBusErrorException in case an error message was received
     * @return the received reply
     */
    static QDBusMessage dbusCall(const QString& method, const QList<QVariant>& args = QList<QVariant>());

    static void diffSnapshotsImpl(const QString& configName, unsigned int snapNum1, unsigned int snapNum2, class ComparisonDoneSignalReceiver *doneSignalReceiver, volatile bool *pRunning);
    static void diffDirectoriesImpl(const QString& subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString& snaprelFilePath, void** moreArgs);
    static void diffDirectoriesRecurse(const QString& subvolume, unsigned int snapNum1, unsigned int snapNum2, const QString& snaprelDirPath, QList<snapper::XFile> *plFiles, class ComparisonDoneSignalReceiver *doneSignalReceiver, volatile bool *pRunning);
};

class ComparisonDoneSignalReceiver
{
    friend SnapperApi;
public:
    virtual ~ComparisonDoneSignalReceiver();

private:
    virtual void comparisonDone(const QList<snapper::XFile>& files) = 0;
    virtual void setComparisonRunningIndicator(bool *pRunning) = 0;
};

}


#endif // SNAPPERAPI_H
