#include <QDebug>

#include <util/util.h>

#include "propertieswindow.h"
#include "ui_propertieswindow.h"
#include "snapperapi.h"
#include "propertieswidget.h"

using snapper::SnapperApi;


PropertiesWindow::PropertiesWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PropertiesWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Properties") + QStringLiteral(" - Snaqqer"));

    // connect to remote signals
    bool success = true;
    success &= SnapperApi::connect(QStringLiteral("ConfigCreated"), this, SLOT(onConfigCreated(QString)));
    success &= SnapperApi::connect(QStringLiteral("ConfigModified"), this, SLOT(onConfigModified(QString)));
    success &= SnapperApi::connect(QStringLiteral("ConfigDeleted"), this, SLOT(onConfigDeleted(QString)));
    if(!success)
    {
        qDebug() << ">>>>> PropertiesWindow:";
        qDebug() << ">>>>> ERROR: failed to connect on or more signals! dynamic updating will not be available.";
        qDebug() << ">>>>>";
    }

    // load configs
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    for(const snapper::XConfigInfo& config : configs)
    {
        onConfigCreated(config.config_name);
    }
}

PropertiesWindow::~PropertiesWindow()
{
    delete ui;
}

void PropertiesWindow::selectTab(int index)
{
    ui->tabWidget->setCurrentIndex(index);
}

void PropertiesWindow::onConfigCreated(const QString &configName)
{
    qDebug() << "PropertiesWindow received config created:" << configName;

    // fetch config data
    SnapperConfig config; // variable is never read
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    for(snapper::XConfigInfo cfg : configs)
    {
        if(cfg.config_name == configName)
        {
            config.config = cfg;
            break;
        }
    }

    // check if we have permission to edit this config
    config.editable = true;
    try
    {
        SnapperApi::getConfig(configName);
    } catch(snapper::DBusErrorException &e) {
        qDebug() << "onConfigCreated:" << configName << e.what();
        // not editable
        config.editable = false;
    }

    // this must happen before adding it to the ui
    snapperConfigNames.append(configName);
    snapperConfigs.append(config);

    // add to ui list (might trigger updatePropertiesUi)
    PropertiesWidget *widget = new PropertiesWidget(config.config, config.editable, ui->tabWidget);
    ui->tabWidget->addTab(widget, config.config.config_name); // QString("%1 (%2)").arg(config.config.config_name, config.config.subvolume)
}

void PropertiesWindow::onConfigModified(const QString& configName)
{
    qDebug() << "PropertiesWindow received config modified:" << configName;

    int index = snapperConfigNames.indexOf(configName);
    SnapperConfig *cfg = &snapperConfigs[index];

    // fetch config data
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    cfg->config = configs[index];

    // check if we still have permission to edit this config
    cfg->editable = true;
    try
    {
        SnapperApi::getConfig(configName);
    } catch(snapper::DBusErrorException &e) {
        qDebug() << "onConfigCreated:" << configName << e.what();
        // not editable
        cfg->editable = false;
    }

    // update the tab with the new data
    PropertiesWidget *widget = qobject_cast<PropertiesWidget*>(ui->tabWidget->widget(index));
    widget->updatePropertiesUi(cfg->config, cfg->editable);
}

void PropertiesWindow::onConfigDeleted(const QString& configName)
{
    qDebug() << "PropertiesWindow received config deleted:" << configName;
    int index = snapperConfigNames.indexOf(configName);
    snapperConfigs.removeAt(index); // TODO sanity check
    snapperConfigNames.removeAt(index);

    // remove from ui
    ui->tabWidget->removeTab(index); // implicitly triggers updatePropertiesUi() through _currentIndexChanged if necessary (hopefully)
}

void PropertiesWindow::closeEvent(QCloseEvent *event)
{
    // check if we have changes and show confirmation dialog
    bool hasChanges = false;
    for(PropertiesWidget *widget : ui->tabWidget->findChildren<PropertiesWidget*>())
    {
        if(widget->hasChanges())
        {
            hasChanges = true;
            break;
        }
    }

    if(!hasChanges)
        event->accept();
    else
    {
        util::SaveChanges action = util::displayConfirmCloseMessage(this);

        if(action == util::SaveChanges::Yes)
        {
            event->setAccepted(saveAll());
        }
        else if(action == util::SaveChanges::No)
        {
            for(PropertiesWidget *widget : ui->tabWidget->findChildren<PropertiesWidget*>())
                widget->discard();
            event->accept();
        }
        else if(action == util::SaveChanges::Cancel)
        {
            event->ignore();
        }
    }
}

void PropertiesWindow::on_buttonBox_clicked(QAbstractButton *button)
{
    QDialogButtonBox::ButtonRole clickedRole = ui->buttonBox->buttonRole(button);

    if(clickedRole == QDialogButtonBox::ButtonRole::AcceptRole)
    {
        // save all clicked
        saveAll();
    }
    else if(clickedRole == QDialogButtonBox::ButtonRole::RejectRole)
    {
        // close clicked
        close();
    }
}

bool PropertiesWindow::saveAll()
{
    bool success = false;
    for(PropertiesWidget *widget : ui->tabWidget->findChildren<PropertiesWidget*>())
        success &= widget->save();
    return success;
}
