#ifndef OLDVERSIONWINDOW_H
#define OLDVERSIONWINDOW_H

#include <QFileInfo>
#include <QMainWindow>
#include "snapperapi.h"

class OldversionsListModel;

namespace Ui {
class OldversionWindow;
}

class OldversionWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OldversionWindow(QString filePath, QWidget *parent = nullptr);
    ~OldversionWindow();

Q_SIGNALS:
    void progress(int numProcessed);
    void done(OldversionsListModel *model, int numTotalSnaps, int numFilteredSnaps);

private Q_SLOTS:
    void onProgress(int numProcessed);
    void onDone(OldversionsListModel *model, int numTotalSnaps, int numFilteredSnaps);

    void updateActions();
    void on_pushButton_open_clicked();
    void on_pushButton_show_clicked();
    void on_pushButton_restore_clicked();
    void on_pushButton_changes_clicked();
    void on_pushButton_diffPrev_clicked();
    void on_pushButton_diffCurr_clicked();

private:
    Ui::OldversionWindow *ui;

    QString m_filePath;
    snapper::XConfigInfo m_snapperConfig;
    QList<QFileInfo> m_fileVersions;

    std::atomic_bool m_alive;
    QFuture<void> m_thread;
    static void createModel(OldversionWindow *self, const QList<snapper::XSnapshot>& snapshots);
    void createModelImpl(const QList<snapper::XSnapshot>& snapshots);

    bool checkFilesDiffer(const QString &snaprelFilePath, unsigned int lastTakenSnapNum, unsigned int iSnapNum, bool ignoreMissing, QFileInfo *pOutFile = nullptr) const;
};

#endif // OLDVERSIONWINDOW_H
