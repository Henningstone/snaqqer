#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractButton>
#include <QTabBar>
#include <QModelIndex>
#include <QVector>

#include "propertieswindow.h"
#include "snapperapi.h"

class TreeItem;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private Q_SLOTS:
    void onConfigCreated(const QString& configName);
    void onConfigModified(const QString &configName);
    void onConfigDeleted(const QString& configName);
    void onSnapshotCreated(const QString& configName, quint32 snapNum);
    void onSnapshotModified(const QString& configName, quint32 snapNum);
    void onSnapshotsDeleted(const QString& configName, snapper::XArray<quint32> snapNumsArg);

    void on_actionExit_triggered();
    void on_actionNewConfiguration_triggered();

    void onTabChanged(int index);
    void onFieldEdited(const QModelIndex &index, const QVariant &newValue);
    void onRowChanged(const QModelIndex &index);
    void updateActions();

    void on_treeView_expanded(const QModelIndex &index);

    void on_toolButton_createNew_clicked();
    void on_toolButton_createNew_triggered(QAction *arg1);
    void on_pushButton_openSnapshot_clicked();
    void on_pushButton_showDiff_clicked();
    void on_pushButton_deleteSnapshot_clicked();
    void on_pushButton_properties_clicked();

private:
    Ui::MainWindow *ui;
    PropertiesWindow propertiesWindow;

    QTabBar *tabBar;

    QList<snapper::XConfigInfo> m_snapperConfigs;

    void rebuildTreeModel();
    void updateUi();
    void showStatusMsg(const QString& message);

    int activeTabIndex() const { return tabBar->currentIndex(); }
    const QString& activeConfigName() const { return m_snapperConfigs[activeTabIndex()].config_name; }
    const QString& activeConfigSubvolume() const { return m_snapperConfigs[activeTabIndex()].subvolume; }
    int tabIndexFromConfigName(const QString &configName) const;
};

#endif // MAINWINDOW_H
