#ifndef PROPERTIESWIDGET_H
#define PROPERTIESWIDGET_H

#include <QWidget>
#include <QAbstractButton>
#include "snapperapi.h"

namespace Ui {
class PropertiesWidget;
}

class PropertiesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PropertiesWidget(const snapper::XConfigInfo& config, bool editable, QWidget *parent = nullptr);
    ~PropertiesWidget();

    void updatePropertiesUi(const snapper::XConfigInfo& config, bool editable);
    snapper::XConfigInfo readPropertiesUi() const;

    bool save();
    void discard();
    bool hasChanges() const;

private Q_SLOTS:
    void on_checkBox_enableTimelineCleanup_toggled(bool checked);
    void on_checkBox_enableNumberCleanup_toggled(bool checked);
    void on_checkBox_enableEmptyCleanup_toggled(bool checked);
    void elementEdited();

    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::PropertiesWidget *ui;

    snapper::XConfigInfo m_config;
};

#endif // PROPERTIESWIDGET_H
