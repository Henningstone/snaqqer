#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include "snapperapi.h"
#include "treeitem.h"


//! [0]
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

Q_SIGNALS:
    void fieldEdited(const QModelIndex &index, const QVariant &newValue);
    void rowChanged(const QModelIndex &index);

public:
    TreeModel(const TreeRowData& headers, const QList<snapper::XSnapshot> &data,
              QObject *parent = nullptr);
    ~TreeModel();
//! [0] //! [1]

    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
//! [1]

//! [2]
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole) override;

    /*bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;*/
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;

    void snapshotCreate(const snapper::XSnapshot& snapshot);
    void snapshotModify(const snapper::XSnapshot& snapshot);
    void snapshotDelete(unsigned int snapNum);
    const TreeRowData& rowData(const QModelIndex &rowIndex);

private:
    void setupModelData(const QList<snapper::XSnapshot> &snapshots, TreeRow *topLevel);
    TreeRow *getItem(const QModelIndex &index) const;
    QModelIndex getIndexBySnapshotNum(unsigned int snapNum, const QModelIndex &parent = QModelIndex());

    TreeRow *rootItem;
};
//! [2]

#endif // TREEMODEL_H
