#include <QDebug>
#include <QDateTime>
#include <util/util.h>
#include "treeitem.h"

using snapper::XSnapshot;


TreeRow::TreeRow(const TreeRowData& data, TreeRow *parent)
    : m_columnData(data),
      m_parentRow(parent)
{
}

TreeRow::~TreeRow()
{
    qDebug() << "TreeRow deleted" << QStringLiteral("%1  %2  %3").arg(m_columnData.num.value<QString>(), m_columnData.date.value<QString>(), m_columnData.desc.value<QString>());
    qDeleteAll(m_childRows);
}

TreeRow *TreeRow::child(int row)
{
    if (row < 0 || row >= m_childRows.size())
        return nullptr;
    return m_childRows.at(row);
}

int TreeRow::myRowNumber() const
{
    if (m_parentRow)
        return m_parentRow->m_childRows.indexOf(const_cast<TreeRow*>(this));
    return 0;
}

void TreeRow::readSnapshot(const snapper::XSnapshot &snapshot)
{
    m_columnData.readSnapshot(snapshot);
}

bool TreeRow::setColumnData(int col, const QVariant &value)
{
    QVariant *target;
    switch(col)
    {
    case COL_NUM: target = &m_columnData.num; break;
    case COL_DATE: target = &m_columnData.date; break;
    case COL_USER: target = &m_columnData.user; break;
    case COL_DESC: target = &m_columnData.desc; break;
    case COL_CLEANUP: target = &m_columnData.cleanup; break;
    case COL_SIZE: target = &m_columnData.size; break;
    default: return false;
    }
    if(*target == value)
        return false;
    *target = value;
    return true;
}

QVariant TreeRow::columnData(int col)
{
    if(col < 0 || col >= NUM_COLS)
        return QVariant();

    return reinterpret_cast<QVariant*>(&m_columnData)[col];
}

void TreeRow::dbgPrint(QString indent)
{
    qDebug() << indent + QStringLiteral("%1  %2  %3").arg(m_columnData.num.value<QString>(), m_columnData.date.value<QString>(), m_columnData.desc.value<QString>());
    for(auto ch : m_childRows)
        ch->dbgPrint(indent + QStringLiteral("    "));
}

bool TreeRow::insertChildren(int position, const QVector<TreeRowData>& datas)
{
    if (position < 0 || position > m_childRows.size())
        return false;

    for (int row = 0; row < datas.count(); row++) {
        TreeRow *newRow = new TreeRow(datas[row], this);
        m_childRows.insert(position, newRow);
    }

    return true;
}

bool TreeRow::removeChildRow(int position)
{
    if (position < 0 || position >= m_childRows.size())
        return false;

    TreeRow *toRemove = m_childRows.takeAt(position);

    // consume its children
    QVector<TreeRowData> children;
    for(int i = 0; i < toRemove->childCount(); i++)
        children << toRemove->child(i)->m_columnData;
    this->insertChildren(position, children);

    delete toRemove;

    return true;
}

void TreeRowData::readSnapshot(const snapper::XSnapshot &snapshot)
{
    num = snapshot.getNum();
    date = snapshot.num == 0 ? QVariant(QStringLiteral("Now")) : snapshot.getDate();
    user = util::usernameFromUID(snapshot.getUid());
    desc = snapshot.getDescription();
    cleanup = snapshot.getCleanup();
    size = -1;
}
