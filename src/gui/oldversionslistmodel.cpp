#include "oldversionslistmodel.h"
#include <QLocale>
#include <QDateTime>
#include <QDir>

OldversionsListModel::OldversionsListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant OldversionsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation != Qt::Orientation::Horizontal)
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        switch(section)
        {
        case COL_NUM: return QStringLiteral("#");
        case COL_DATE: return tr("Date");
        case COL_DESC: return tr("Description");
        case COL_SIZE: return tr("Size");
        case COL_MTIME: return tr("Modified");
        default: Q_ASSERT(false);
        }
    }
    else if(role == Qt::TextAlignmentRole)
    {
        if(section == COL_NUM)
            return ((int)Qt::AlignRight | Qt::AlignVCenter);
    }

    return QVariant();
}

QModelIndex OldversionsListModel::index(int row, int column, const QModelIndex &parent) const
{
    if(parent.isValid())
        return QModelIndex();

    if(row < 0 || row > m_listRows.size() || column < 0 || column > NUM_COLS)
        return QModelIndex();

    return createIndex(row, column, const_cast<void*>(reinterpret_cast<const void*>(&m_listRows[row])));
}

int OldversionsListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_listRows.size();
}

QVariant OldversionsListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // see notice in rowCount above
    if (index.parent().isValid())
        return QVariant();

    if(index.row() < 0 || index.row() > rowCount())
        return QVariant();

    if(index.column() < 0 || index.column() > NUM_COLS)
        return QVariant();


    const ListRowData& data = m_listRows[index.row()];

    if (role == Qt::DisplayRole)
    {
        if(index.column() == COL_SIZE)
        {
            if(data.file.isDir())
            {
                return QVariant::fromValue<QString>(QString::number(QDir(data.file.absoluteFilePath()).count()) + QStringLiteral(" ") + tr("Elements"));
            }
            else
            {
                QLocale loc;
                return loc.formattedDataSize(data.size.value<qint64>());
            }
        }
        else if(index.column() == COL_MTIME)
            return data.file.fileTime(QFile::FileTime::FileModificationTime);
        else
            return reinterpret_cast<const QVariant*>(&data)[index.column()];
    }
    else if(role == Qt::ToolTipRole)
    {
        if(index.column() == COL_SIZE)
        {
            if(data.file.isFile())
                return QVariant::fromValue<QString>(QString::number(data.size.value<qint64>()) + QStringLiteral(" ") + tr("bytes"));
        }
        else if(index.column() == COL_MTIME)
        {
            return data.file.fileTime(QFile::FileTime::FileMetadataChangeTime);
        }
    }
    else if(role == Qt::TextAlignmentRole)
    {
        if(index.column() == COL_NUM)
            return ((int)Qt::AlignRight | Qt::AlignVCenter);
    }
    else if(role == Qt::DecorationRole)
    {
        if(index.column() == COL_NUM)
        return m_icons.icon(data.file);
    }

    return QVariant();
}

void OldversionsListModel::addFileInfo(const snapper::XSnapshot &snapshot, const QFileInfo &file)
{
    ListRowData newRow;
    newRow.num = snapshot.getNum();
    newRow.date = snapshot.getDate();
    newRow.desc = snapshot.getDescription();
    newRow.size = file.size();
    newRow.file = file;

    beginInsertRows(QModelIndex(), m_listRows.size(), m_listRows.size());
    m_listRows.append(newRow);
    endInsertRows();
}
