#include <QDebug>
#include <QDir>
#include <QtConcurrent/QtConcurrent>

#include <util/shared.h>
#include <util/util.h>

#include "oldversionwindow.h"
#include "ui_oldversionwindow.h"
#include "oldversionslistmodel.h"
#include "diffwindow.h"
#include "snapperapi.h"

using snapper::SnapperApi;


OldversionWindow::OldversionWindow(QString filePath, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OldversionWindow)
{
    ui->setupUi(this);
    if(parent)
        setAttribute(Qt::WA_DeleteOnClose);

    ui->frame->hide();

    // connect to remote signals
    bool success = true;
    success &= SnapperApi::connect(QStringLiteral("ConfigDeleted"), this, SLOT(onConfigDeleted(QString)));
    success &= SnapperApi::connect(QStringLiteral("SnapshotCreated"), this, SLOT(onSnapshotCreated(QString, quint32)));
    //success &= SnapperApi::connect("SnapshotModified", this, SLOT(onSnapshotModified(QString, quint32)));
    success &= SnapperApi::connect(QStringLiteral("SnapshotsDeleted"), this, SLOT(onSnapshotsDeleted(QString, snapper::XArray<quint32>)));
    if(!success)
    {
        qDebug() << ">>>>> OldversionWindow";
        qDebug() << ">>>>> ERROR: failed to connect on or more signals! dynamic updating will not be available.";
        qDebug() << ">>>>>";
    }

    // check if this path is under a symlink
    QString canonicalPath = QFileInfo(filePath).canonicalFilePath();
    qDebug() << "canonical" << canonicalPath;
    if(canonicalPath != filePath)
    {
        if(util::displayQuestionMessage(this, tr("Use canonical path?"),
                tr("The file path you requested is accessed through a symlink. This might lead to unexpected behaviour.\n"
                   "Do you want to use the canonical (real) path instead?") + QStringLiteral("\n\n") +
                   tr("Requested path: ") + filePath + QStringLiteral("\n\n") +
                   tr("Canonical path: ") + canonicalPath))
            filePath = canonicalPath;
    }


    QFileInfo inputPath(filePath);
    qDebug() << filePath
             << ", isFile" << inputPath.isFile()
             << ", isRelative" << inputPath.isRelative()
             << ", isAbsolute" << inputPath.isAbsolute();
    m_filePath = inputPath.absoluteFilePath();
    qDebug() << "==>" << m_filePath;

    ui->label_path->setText(m_filePath);

    // load configs and check if our file is to be found in one
    QList<snapper::XConfigInfo> configs = SnapperApi::listConfigs();
    int longestMatch = 0;
    for(const snapper::XConfigInfo& config : configs)
    {
        if(m_filePath.startsWith(config.subvolume) && config.subvolume.length() > longestMatch)
        {
            // found a match
            longestMatch = config.subvolume.length();
            m_snapperConfig = config;
        }
    }

    if(longestMatch <= 0)
    {
        util::displayErrorMessage(this,
                                  tr("File not under snapshotting"),
                                  tr("The selected file '%1' is not covered by any snapper configuration.").arg(m_filePath), false);
        close();
        return;
    }

    // fire off file comparison
    connect(this, &OldversionWindow::progress, this, &OldversionWindow::onProgress);
    connect(this, &OldversionWindow::done, this, &OldversionWindow::onDone);

    QList<snapper::XSnapshot> snapshots = SnapperApi::listSnapshots(m_snapperConfig.config_name);
    ui->progressBar->setRange(0, snapshots.length()-1);
    m_alive = true;
    QtConcurrent::run(createModel, this, snapshots);
}

OldversionWindow::~OldversionWindow()
{
    m_alive = false;
    qDebug() << "OldversionWindow dtor waiting for createModel thread...";
    m_thread.waitForFinished();
    qDebug() << "> OldversionWindow done waiting";
    delete ui;
}

void OldversionWindow::createModel(OldversionWindow *self, const QList<snapper::XSnapshot>& snapshots)
{
    self->createModelImpl(snapshots);
    qDebug() << "> createModel thread finished";
}

void OldversionWindow::createModelImpl(const QList<snapper::XSnapshot>& snapshots)
{
    // create model
    OldversionsListModel *model = new OldversionsListModel();

    // load snapshots and filter them
    int numFilteredSnaps = 0;
    int lastTaken = 0;
    for(int i = snapshots.length() - 1; i >= 1; i--)
    {
        if(!m_alive)
            return;

        Q_EMIT progress(snapshots.length()-1 - i);

        // check if the file version in this snapshot differs from the version in the last taken snapshot above
        /*int compareSnapNum;
        if(i == snapshots.length())
            compareSnapNum = 0; // compare the latest snapshot with the current situation
        else
            compareSnapNum = (int)snapshots[i-1].getNum();*/

        qDebug() << "recursive stat compare" << snapshots[i].num << "with" << snapshots[lastTaken].num;

        QFileInfo file;
        bool filesDiffer = checkFilesDiffer(
                    m_filePath.mid(m_snapperConfig.subvolume.length()),
                    snapshots[lastTaken].getNum(),
                    snapshots[i].getNum(),
                    true, &file
        );

        if(filesDiffer)
        {
            lastTaken = i;

            // add it to the list
            m_fileVersions.append(file);
            model->addFileInfo(snapshots[i], file);
            qDebug() << "take" << snapshots[i].num;
        }
        else
        {
            numFilteredSnaps++;
            qDebug() << "ignore" << snapshots[i].num;
        }
    }

    model->moveToThread(ui->treeView->thread());
    Q_EMIT done(model, snapshots.length(), numFilteredSnaps);
}

void OldversionWindow::onProgress(int numProcessed)
{
    ui->progressBar->setValue(numProcessed);
}

void OldversionWindow::onDone(OldversionsListModel *model, int numTotalSnaps, int numFilteredSnaps)
{
    model->setParent(ui->treeView);
    ui->treeView->setModel(model);

    QFrame *separator1 = new QFrame(ui->statusbar);
    QFrame *separator2 = new QFrame(ui->statusbar);
    separator1->setFrameShape(QFrame::VLine);
    separator2->setFrameShape(QFrame::VLine);
    separator1->setFrameShadow(QFrame::Sunken);
    separator2->setFrameShadow(QFrame::Sunken);
    int total = numTotalSnaps - 1; // we don't display snapshot 0
    ui->statusbar->addWidget(new QLabel(tr("Shown: %1").arg(QString::number(total - numFilteredSnaps)), ui->statusbar));
    ui->statusbar->addWidget(separator1);
    ui->statusbar->addWidget(new QLabel(tr("Hidden: %1").arg(QString::number(numFilteredSnaps)), ui->statusbar));
    ui->statusbar->addWidget(separator2);
    ui->statusbar->addWidget(new QLabel(tr("Total: %1").arg(QString::number(total)), ui->statusbar));

    // auto-resize all columns
    for (int column = 0; column < model->columnCount(); column++) {
        ui->treeView->resizeColumnToContents(column);
    }

    // listen for selection
    connect(ui->treeView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &OldversionWindow::updateActions);

    ui->frame_progress->hide();
    ui->frame->show();
}

bool OldversionWindow::checkFilesDiffer(const QString &snaprelFilePath, unsigned int lastTakenSnapNum, unsigned int iSnapNum, bool ignoreMissing, QFileInfo *pOutFile) const
{
    if(!m_alive)
        return false;

    // open the compare file in the compare snapshot
    QFileInfo compareFile(util::snapshotPath(m_snapperConfig.subvolume, lastTakenSnapNum, snaprelFilePath));

    // open the file in the snapshot
    QFileInfo file(util::snapshotPath(m_snapperConfig.subvolume, iSnapNum, snaprelFilePath));

    if(pOutFile)
        *pOutFile = file;

    if(ignoreMissing && !file.exists())
        return false;

    // comparing MACB information and size should be well enough
    bool filesDiffer = file.exists() != compareFile.exists() || file.isDir() != compareFile.isDir()
            || file.fileTime(QFile::FileTime::FileModificationTime) != compareFile.fileTime(QFile::FileTime::FileModificationTime)
            //|| file.fileTime(QFile::FileTime::FileAccessTime) != compareFile.fileTime(QFile::FileTime::FileAccessTime)
            || file.fileTime(QFile::FileTime::FileMetadataChangeTime) != compareFile.fileTime(QFile::FileTime::FileMetadataChangeTime)
            || file.fileTime(QFile::FileTime::FileBirthTime) != compareFile.fileTime(QFile::FileTime::FileBirthTime)
            || file.size() != compareFile.size();

    // recurse into directories if necessary
    if(!filesDiffer && file.isDir())
    {
        // until we find a differing file within the current directory
        QStringList dirList = QDir(file.filePath()).entryList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::DirsLast);
        for(const QString &name : dirList)
        {
            filesDiffer = checkFilesDiffer(snaprelFilePath + QStringLiteral("/") + name, lastTakenSnapNum, iSnapNum, false);
            if(filesDiffer)
                return true;
        }
    }

    return filesDiffer;
}

void OldversionWindow::updateActions()
{
    const QModelIndex& currentIndex = ui->treeView->selectionModel()->currentIndex();
    const bool hasCurrent = currentIndex.isValid();
    const bool lastIsSelected = currentIndex.row() == qobject_cast<OldversionsListModel*>(ui->treeView->model())->rowCount() - 1;

    ui->pushButton_changes->setEnabled(hasCurrent && !lastIsSelected);
    ui->pushButton_diffPrev->setEnabled(hasCurrent && !lastIsSelected);
    ui->pushButton_diffCurr->setEnabled(hasCurrent);
    ui->pushButton_open->setEnabled(hasCurrent);
    ui->pushButton_show->setEnabled(hasCurrent);
    ui->pushButton_restore->setEnabled(hasCurrent);
}

void OldversionWindow::on_pushButton_open_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    QString selectedfileVersionPath = m_fileVersions[selected.row()].absoluteFilePath();
    qDebug() << "open" << selectedfileVersionPath;
    util::openPath(selectedfileVersionPath);
}

void OldversionWindow::on_pushButton_show_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    QString selectedfileVersionPath = m_fileVersions[selected.row()].absoluteFilePath();
    qDebug() << "show" << selectedfileVersionPath;
    util::openFolderSelect(selectedfileVersionPath);
}

void OldversionWindow::on_pushButton_restore_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    OldversionsListModel *model = qobject_cast<OldversionsListModel*>(ui->treeView->model());

    unsigned int snapNum = model->data(model->index(selected.row(), OldversionsListModel::COL_NUM)).value<unsigned int>();
    QDateTime date = model->data(model->index(selected.row(), OldversionsListModel::COL_DATE)).value<QDateTime>();

    //XXX QString selectedfileVersionPath = m_fileVersions[selected.row()].absoluteFilePath();
    restoreFileFromSnap(this, m_filePath, m_snapperConfig, snapNum, date);
}

void OldversionWindow::on_pushButton_changes_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    OldversionsListModel *model = qobject_cast<OldversionsListModel*>(ui->treeView->model());

    unsigned int snapNum = model->data(model->index(selected.row(), OldversionsListModel::COL_NUM)).value<unsigned int>();

    if(selected.row() == model->rowCount() - 1)
    {
        qDebug() << "[ERROR] OldversionWindow: on_pushButton_changes_clicked() for last row, ignoring";
        return;
    }

    unsigned int snapNumPrev = model->data(model->index(selected.row() + 1, OldversionsListModel::COL_NUM)).value<unsigned int>();

    new DiffWindow(m_snapperConfig, snapNumPrev, snapNum, this, &m_filePath);
}

void OldversionWindow::on_pushButton_diffPrev_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    OldversionsListModel *model = qobject_cast<OldversionsListModel*>(ui->treeView->model());

    unsigned int snapNum = model->data(model->index(selected.row(), OldversionsListModel::COL_NUM)).value<unsigned int>();

    if(selected.row() == model->rowCount() - 1)
    {
        qDebug() << "[ERROR] OldversionWindow: on_pushButton_diffPrev_clicked() for last row, ignoring";
        return;
    }

    unsigned int snapNumPrev = model->data(model->index(selected.row() + 1, OldversionsListModel::COL_NUM)).value<unsigned int>();
    new DiffWindow(m_snapperConfig, snapNumPrev, snapNum, this);
}

void OldversionWindow::on_pushButton_diffCurr_clicked()
{
    const QModelIndex& selected = ui->treeView->currentIndex();
    if(!selected.isValid())
        return;

    OldversionsListModel *model = qobject_cast<OldversionsListModel*>(ui->treeView->model());

    unsigned int snapNum = model->data(model->index(selected.row(), OldversionsListModel::COL_NUM)).value<unsigned int>();
    new DiffWindow(m_snapperConfig, snapNum, 0, this);

}
