#include <QDebug>
#include <QMessageBox>
#include <QLayout>
#include <QDesktopServices>
#include <QDir>
#include <QProcess>
#include <QRegularExpression>
#include <QUrl>

#include <KLocalizedString>

#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>

#include <qBittorrent/base/utils/version.h>
#include "util.h"


namespace util {

bool displayErrorMessage(QWidget *parent, const QString &title, const QString &message, bool offerRetry)
{
    QMessageBox msg(parent);
    // TODO fixme
    //msg.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //msg.layout()->setGeometry(QRect(0, 0, 700, 300));
    msg.setModal(true);
    msg.setWindowModality(Qt::WindowModality::ApplicationModal);
    msg.setIcon(QMessageBox::Critical);
    msg.setWindowTitle(xi18nc("@title:window", "Error"));
    msg.setText(title);
    msg.setInformativeText(message);

    if(offerRetry)
        msg.setStandardButtons(QMessageBox::Retry | QMessageBox::Ignore);
    else
        msg.setStandardButtons(QMessageBox::Ignore);
    msg.setDefaultButton(QMessageBox::Ignore);

    return msg.exec() == QMessageBox::Retry;
}

bool displayQuestionMessage(QWidget *parent, const QString &title, const QString &message)
{
    QMessageBox msg(parent);
    // TODO fixme
    //msg.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //msg.layout()->setGeometry(QRect(0, 0, 700, 300));
    msg.setModal(true);
    msg.setWindowModality(Qt::WindowModality::ApplicationModal);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowTitle(xi18nc("@title:window", "Question"));
    msg.setText(title);
    msg.setInformativeText(message);
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    return msg.exec() == QMessageBox::Yes;
}

void displayInfoMessage(QWidget *parent, const QString &title, const QString &message)
{
    QMessageBox msg(parent);
    // TODO fixme
    //msg.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //msg.layout()->setGeometry(QRect(0, 0, 700, 300));
    msg.setModal(true);
    msg.setWindowModality(Qt::WindowModality::ApplicationModal);
    msg.setIcon(QMessageBox::Information);
    msg.setWindowTitle(xi18nc("@title:window", "Information"));
    msg.setText(title);
    msg.setInformativeText(message);
    msg.setStandardButtons(QMessageBox::Ok);

    msg.exec();
}

SaveChanges displayYesNoCancelMessage(QWidget *parent, const QString &title, const QString &message)
{
    QMessageBox msg(parent);
    // TODO fixme
    //msg.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //msg.layout()->setGeometry(QRect(0, 0, 700, 300));
    msg.setModal(true);
    msg.setWindowModality(Qt::WindowModality::ApplicationModal);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowTitle(xi18nc("@title:window", "Question"));
    msg.setText(title);
    msg.setInformativeText(message);
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    int result = msg.exec();
    switch(result)
    {
    case QMessageBox::Yes: return SaveChanges::Yes;
    case QMessageBox::No: return SaveChanges::No;
    case QMessageBox::Cancel: return SaveChanges::Cancel;
    default: Q_ASSERT(false);
    }
}

SaveChanges displayConfirmCloseMessage(QWidget *parent)
{
    QMessageBox msg(parent);
    // TODO fixme
    //msg.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //msg.layout()->setGeometry(QRect(0, 0, 700, 300));
    msg.setModal(true);
    msg.setWindowModality(Qt::WindowModality::ApplicationModal);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowTitle(xi18nc("@title:window", "Confirm close"));
    msg.setText(xi18nc("@title:info", "You have unsaved changes."));
    msg.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

    int result = msg.exec();
    switch(result)
    {
    case QMessageBox::Save: return SaveChanges::Yes;
    case QMessageBox::Discard: return SaveChanges::No;
    case QMessageBox::Cancel: return SaveChanges::Cancel;
    default: Q_ASSERT(false);
    }
}

static QMap<uid_t, QString> usernameCache;
QString usernameFromUID(uid_t uid)
{
    if(usernameCache.contains(uid))
        return usernameCache[uid];

    QString username = QString::fromLocal8Bit(getpwuid(uid)->pw_name);
    usernameCache[uid] = username;
    return username;
}

QString currentUserName()
{
    return QString::fromLocal8Bit(getlogin());
}

QString smartCompleteCleanup(QString prefix)
{
    if(prefix.trimmed() == QStringLiteral(""))
        return QStringLiteral("");
    // smart completion
    if(QStringLiteral("timeline").startsWith(prefix))
        prefix = QStringLiteral("timeline");
    else if(QStringLiteral("number").startsWith(prefix))
        prefix = QStringLiteral("number");
    else if(QStringLiteral("empty-pre-post").startsWith(prefix))
        prefix = QStringLiteral("empty-pre-post");
    return prefix;
}

bool isValidCleanup(const QString &algoName)
{
    return algoName == QStringLiteral("") ||
            algoName == QStringLiteral("timeline") ||
            algoName == QStringLiteral("number") ||
            algoName == QStringLiteral("empty-pre-post");
}

QString snapshotPath(const QString &subvolume, unsigned int snapNum, QString containedObject)
{
    if(containedObject == QStringLiteral(""))
        containedObject = QStringLiteral(".");

    containedObject = snapshotPathRelativify(subvolume, snapNum, containedObject);

    if(snapNum == 0)
        return QFileInfo(subvolume + QStringLiteral("/") + containedObject).absoluteFilePath();
    else
        return QFileInfo(QStringLiteral("%1/.snapshots/%2/snapshot/%3").arg(subvolume, QString::number(snapNum), containedObject)).absoluteFilePath();
}

QString snapshotPathRelativify(QString subvolume, unsigned int snapNum, QString absoluteFilePath)
{
    if(subvolume == QStringLiteral("/"))
        subvolume = QStringLiteral("");
    QString prefix;
    if(snapNum == 0)
        prefix = subvolume + QStringLiteral("/");
    else
        prefix = QStringLiteral("%1/.snapshots/%2/snapshot/").arg(subvolume, QString::number(snapNum));

    if(absoluteFilePath.startsWith(prefix))
        return absoluteFilePath.mid(prefix.length());
    else if(absoluteFilePath.startsWith(subvolume))
        return absoluteFilePath.mid(subvolume.length() + 1);
    return absoluteFilePath;
}

bool btrfsSubvolumeCreate(const QString& path, QByteArray *pOutProcessOutput)
{
    QProcess *procCp = new QProcess();
    procCp->start(QStringLiteral("btrfs"), { QStringLiteral("subvolume"), QStringLiteral("create"), path });
    if(!procCp->waitForFinished(4500))
        return false;

    if(pOutProcessOutput)
        *pOutProcessOutput = procCp->readAll();

    return procCp->exitStatus() == QProcess::ExitStatus::NormalExit;
}


/*
 * the following code is adapted from qBittorrent
 * https://github.com/qbittorrent/qBittorrent/blob/ec33cdd8b57153ee298e5f43c7a9b29ea3e0552d/src/gui/utils.cpps
 */

void openPath(const QString &absolutePath)
{
    qDebug() << "openPath" << absolutePath;

    const QString path = QDir::fromNativeSeparators(absolutePath);
    // Hack to access samba shares with QDesktopServices::openUrl
    if (path.startsWith(QStringLiteral("//")))
        QDesktopServices::openUrl(QUrl::fromLocalFile(QDir::toNativeSeparators(QStringLiteral("file:") + path)));
    else
        QDesktopServices::openUrl(QUrl::fromLocalFile(path));
}

// Open the parent directory of the given path with a file manager and select
// (if possible) the item at the given path
void openFolderSelect(const QString &absolutePath)
{
    const QString path = QDir::fromNativeSeparators(absolutePath);
    // If the item to select doesn't exist, try to open its parent
    if (!QFileInfo::exists(path)) {
        openPath(path.left(path.lastIndexOf(QChar::fromLatin1('/'))));
        return;
    }
#ifdef Q_OS_WIN
#error not implemented
#elif defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
    QProcess proc;
    proc.start(QStringLiteral("xdg-mime"), {QStringLiteral("query"), QStringLiteral("default"), QStringLiteral("inode/directory")});
    proc.waitForFinished();
    const QString output = QString::fromLocal8Bit(proc.readLine().simplified());
    if ((output == QStringLiteral("dolphin.desktop")) || (output == QStringLiteral("org.kde.dolphin.desktop"))) {
        proc.startDetached(QStringLiteral("dolphin"), {QStringLiteral("--select"), QDir::toNativeSeparators(path)});
    }
    else if ((output == QStringLiteral("nautilus.desktop")) || (output == QStringLiteral("org.gnome.Nautilus.desktop"))
                 || (output == QStringLiteral("nautilus-folder-handler.desktop"))) {
        proc.start(QStringLiteral("nautilus"), {QStringLiteral("--version")});
        proc.waitForFinished();
        const QString nautilusVerStr = QString::fromLocal8Bit(proc.readLine()).remove(QRegularExpression(QStringLiteral("[^0-9.]")));
        using NautilusVersion = Utils::Version<int, 3>;
        if (NautilusVersion::tryParse(nautilusVerStr, {1, 0, 0}) > NautilusVersion {3, 28})
            proc.startDetached(QStringLiteral("nautilus"), {QDir::toNativeSeparators(path)});
        else
            proc.startDetached(QStringLiteral("nautilus"), {QStringLiteral("--no-desktop"), QDir::toNativeSeparators(path)});
    }
    else if (output == QStringLiteral("nemo.desktop")) {
        proc.startDetached(QStringLiteral("nemo"), {QStringLiteral("--no-desktop"), QDir::toNativeSeparators(path)});
    }
    else if ((output == QStringLiteral("konqueror.desktop")) || (output == QStringLiteral("kfmclient_dir.desktop"))) {
        proc.startDetached(QStringLiteral("konqueror"), {QStringLiteral("--select"), QDir::toNativeSeparators(path)});
    }
    else {
        // "caja" manager can't pinpoint the file, see: https://github.com/qbittorrent/qBittorrent/issues/5003
        openPath(path.left(path.lastIndexOf(QChar::fromLatin1('/'))));
    }
#else
    openPath(path.left(path.lastIndexOf(QChar::fromLatin1('/'))));
#endif
}

}
