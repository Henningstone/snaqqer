#include <QString>
#include <QWidget>
#include <gui/snapperapi.h>

void restoreFileFromSnap(QWidget *parent, const QString& filePath, const snapper::XConfigInfo &snapperConfig, unsigned int snapNum, QDateTime date);
