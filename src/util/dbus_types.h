#ifndef DBUS_TYPES_H
#define DBUS_TYPES_H

#include <QMetaType>
#include <QDBusArgument>
#include <QDateTime>

namespace snapper {
Q_NAMESPACE

enum SnapshotType { SINGLE, PRE, POST };
Q_ENUM_NS(SnapshotType)

// https://github.com/openSUSE/snapper/blob/baf4ce877c5e27d5b01f8a660c5e52d5f0e74ff0/snapper/File.h#L39-L51
enum FileStatusFlags
{
    // 1..4 are mutually exclusive
    CREATED = 1u,      // created
    DELETED = 2u,      // deleted
    _TYPE = 4u,         // type has changed (file/folder)
    _CONTENT = 8u,      // content has changed
    CHANGED = 8u, // combines TYPE and CONTENT

    PERMISSIONS = 16u, // permissions have changed, see chmod(2)
    OWNER = 32u,       // owner has changed, see chown(2)
    GROUP = 64u,       // group has changed, see chown(2)
    XATTRS = 128u,     // extended attributes changed, see attr(5)
    ACL = 256u,        // access control list changed, see acl(5)

    //CHANGED = TYPE | CONTENT,
    META = PERMISSIONS | OWNER | GROUP //| XATTRS | ACL // some metadata has changed
};
Q_ENUM_NS(FileStatusFlags)

struct XConfigInfo
{
    QString config_name;
    QString subvolume;

    QMap<QString, QString> raw;

    bool operator!=(const XConfigInfo& other);
};

struct XSnapshot
{
    quint16 getType() const { return type; }

    unsigned int getNum() const { return num; }

    QDateTime getDate() const { return QDateTime::fromSecsSinceEpoch(date); }

    uid_t getUid() const { return uid; }

    unsigned int getPreNum() const { return pre_num; }

    const QString& getDescription() const { return description; }

    const QString& getCleanup() const { return cleanup; }

    const QMap<QString, QString>& getUserdata() const { return userdata; }

    quint16 type;
    unsigned int num;
    qint64 date;
    uid_t uid;
    unsigned int pre_num;
    QString description;
    QString cleanup;
    QMap<QString, QString> userdata;
};

struct XFile
{
    QString name;
    unsigned int status;
};

template <typename T>
struct XArray
{
    QList<T> data;
};

}


// global types

void snapper_dbus_types_init();

Q_DECLARE_METATYPE(snapper::XConfigInfo)
QDBusArgument &operator<<(QDBusArgument &argument, const snapper::XConfigInfo &mystruct);
const QDBusArgument &operator>>(const QDBusArgument &argument, snapper::XConfigInfo &mystruct);

Q_DECLARE_METATYPE(snapper::XSnapshot)
QDBusArgument &operator<<(QDBusArgument &argument, const snapper::XSnapshot &mystruct);
const QDBusArgument &operator>>(const QDBusArgument &argument, snapper::XSnapshot &mystruct);

Q_DECLARE_METATYPE(snapper::XFile)
QDBusArgument &operator<<(QDBusArgument &argument, const snapper::XFile &mystruct);
const QDBusArgument &operator>>(const QDBusArgument &argument, snapper::XFile &mystruct);

Q_DECLARE_METATYPE(snapper::XArray<quint32>)
template<typename T> QDBusArgument &operator<<(QDBusArgument &argument, const snapper::XArray<T> &mystruct);
template<typename T> const QDBusArgument &operator>>(const QDBusArgument &argument, snapper::XArray<T> &mystruct);


#endif // DBUS_TYPES_H
