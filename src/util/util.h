#ifndef UTIL_H
#define UTIL_H
#include <QString>
#include <QMessageBox>

namespace util {

enum SaveChanges { No, Yes, Cancel };

/**
 * @brief displays a QMessageBox with an error and the button ignore and optionally retry
 * @param parent parent widget
 * @param title message box title e.g. error brief
 * @param message error message
 * @param offerRetry whether to show a retry button
 * @return true if the user clicked retry, false for ignore
 */
bool displayErrorMessage(QWidget *parent, const QString &title, const QString &message, bool offerRetry);
bool displayQuestionMessage(QWidget *parent, const QString &title, const QString &message);
void displayInfoMessage(QWidget *parent, const QString &title, const QString &message);
SaveChanges displayYesNoCancelMessage(QWidget *parent, const QString &title, const QString &message);
SaveChanges displayConfirmCloseMessage(QWidget *parent);

QString usernameFromUID(uid_t uid);
QString currentUserName();

/**
 * @brief completes the beginning of a cleanup algorithm to its full name
 * @param prefix a string which might be the start of a cleanup algorithm name
 * @return the full name if it was, otherwise the original string
 */
QString smartCompleteCleanup(QString prefix);

bool isValidCleanup(const QString &algoName);

QString snapshotPath(const QString& subvolume, unsigned int snapNum, QString containedObject = QStringLiteral(""));
QString snapshotPathRelativify(QString subvolume, unsigned int snapNum, QString absoluteFilePath);

bool btrfsSubvolumeCreate(const QString& path, QByteArray *pOutProcessOutput);

void openPath(const QString &absolutePath);
void openFolderSelect(const QString &absolutePath);

}


#endif // UTIL_H
