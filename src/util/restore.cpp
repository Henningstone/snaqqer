#include <QMap>
#include <QDir>
#include "util.h"
#include <gui/snapperapi.h>
#include "shared.h"

using namespace snapper;


void restoreFileFromSnap(QWidget *parent, const QString& filePath, const XConfigInfo& snapperConfig, unsigned int snapNum, QDateTime date)
{
    if(QFileInfo(filePath).isDir())
    {
        if(!util::displayQuestionMessage(parent, QStringLiteral("WARNING: About to restore a directory"),
                                     QStringLiteral("WARNING: any subvolumes contained in this directory are not present in snapshots and can thus not be restored.\n"
                                                "This means they WILL BE LOST, IRREVERSIBLY! Before proceeding, be sure that there are no subvolumes in this directory.")))
            return;
    }

    util::SaveChanges answer = util::displayYesNoCancelMessage(parent,
                                    QStringLiteral("Create new snapshot before restoring old version?"),
                                    QStringLiteral("You are about to replace '%1' with how it appeared in snapshot %2 on %3.\n"
                                       "Do you want to retain the current version by creating a new snapshot now?").arg(filePath, QString::number(snapNum), date.toString()));
    if(answer == util::Yes)
    {
        QMap<QString, QString> userdata;
        userdata[QStringLiteral("Restored file path")] = filePath;
        SnapperApi::createSingleSnapshot(snapperConfig.config_name,
                                         QStringLiteral("Restore '%1' from %2 (%3)").arg(filePath.split(QChar::fromLatin1('/')).last(),
                                                                             QString::number(snapNum), date.toString()),
                                         QStringLiteral(""), userdata);
    }

    else if(answer == util::Cancel)
        return;

    // make a backup
    QString backupFile = filePath + QStringLiteral(".snaqqerbak.") + QString::number(rand()%(99999+1-10000)+10000) + QStringLiteral("~");
    QFile::rename(filePath, backupFile);

    // run cp --reflink=always -r <snapshot> <filepath>
    QProcess *procCp = new QProcess();
    parent->connect(procCp, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus){
        if(exitStatus == QProcess::NormalExit && exitCode == 0)
        {
            // remove the backup
            bool backupRemoved = QDir(backupFile).removeRecursively();
            QString message = QStringLiteral("The selected element was restored to its previous version.");
            if(!backupRemoved)
                message += QStringLiteral("\n\n") + QStringLiteral("However, the backup couldn't be removed. Please delete this manually: '%1'").arg(backupFile);

            util::displayInfoMessage(parent, QStringLiteral("Restore successful"), message);
        }
        else
        {
            util::displayErrorMessage(parent, QStringLiteral("Restore failed"),
                                      QStringLiteral("The process encountered an error:") + QString::fromUtf8(procCp->readAllStandardError()) + QStringLiteral("\n\n") +
                                      QStringLiteral("The previous version still exists as %1").arg(backupFile) + QStringLiteral("\n\n") +
                                      QStringLiteral("Please check the situation manually."), false);
        }
        delete procCp;
    });
    procCp->start(QStringLiteral("cp"), {
                      QStringLiteral("--reflink=always"), QStringLiteral("-r"),
                      util::snapshotPath(snapperConfig.subvolume, snapNum, util::snapshotPathRelativify(snapperConfig.subvolume, 0, filePath)),
                      filePath
    });

    // note: we don't use "snapper undochange <number1>..<number2> [files]" because it's overkill for the task.
    // it takes way too long and does way too many things it doesn't need to inorder to restore a single file.
}
