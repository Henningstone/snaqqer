#include <QDBusMetaType>
#include "dbus_types.h"

using namespace snapper;


bool XConfigInfo::operator!=(const XConfigInfo &other)
{
    if(config_name != other.config_name || subvolume != other.subvolume)
        return true;
    for(const QString& key : raw.keys())
    {
        if(raw[key] != other.raw[key])
            return true;
    }
    return false;
}


void snapper_dbus_types_init()
{
    qDBusRegisterMetaType<XConfigInfo>();
    qDBusRegisterMetaType<XSnapshot>();
    qDBusRegisterMetaType<XFile>();
    qDBusRegisterMetaType<XArray<quint32>>();

    qDBusRegisterMetaType<QList<XFile>>();
}


QDBusArgument &operator<<(QDBusArgument &argument, const XConfigInfo &mystruct)
{
    argument.beginArray(qMetaTypeId<XConfigInfo>());
    argument.beginStructure();
    argument << mystruct.config_name << mystruct.subvolume << mystruct.raw;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, XConfigInfo &mystruct)
{
    argument.beginStructure();
    argument >> mystruct.config_name >> mystruct.subvolume >> mystruct.raw;
    argument.endStructure();
    return argument;
}

//

QDBusArgument &operator<<(QDBusArgument &argument, const XSnapshot &mystruct)
{
    argument.beginStructure();
    argument
            << mystruct.num
            //<< static_cast<const qint16&>(mystruct.type)
            //<< mystruct.pre_num
            //<< static_cast<const qint64&>(mystruct.date)
            //<< mystruct.uid
            << mystruct.description
            << mystruct.cleanup
            << mystruct.userdata;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, XSnapshot &mystruct)
{
    argument.beginStructure();
    argument
            >> mystruct.num
            >> mystruct.type
            >> mystruct.pre_num
            >> mystruct.date
            >> mystruct.uid
            >> mystruct.description
            >> mystruct.cleanup
            >> mystruct.userdata;
    argument.endStructure();
    return argument;
}

//

QDBusArgument &operator<<(QDBusArgument &argument, const XFile &mystruct)
{
    argument.beginStructure();
    argument
            << mystruct.name
            << mystruct.status;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, XFile &mystruct)
{
    argument.beginStructure();
    argument
            >> mystruct.name
            >> mystruct.status;
    argument.endStructure();
    return argument;
}

template <typename T>
QDBusArgument &operator<<(QDBusArgument &argument, const XArray<T> &mystruct)
{
    argument << mystruct.data;
    return argument;
}

template <typename T>
const QDBusArgument &operator>>(const QDBusArgument &argument, XArray<T> &mystruct)
{
    argument >> mystruct.data;
    return argument;
}


/*
    REMINDER: when adding more marshalling, don't forget to
              add the bit at the bottom of dbus_types.h and
              in snapper_dbus_types_init at the top in here
*/
