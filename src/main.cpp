#include "gui/mainwindow.h"
#include "gui/oldversionwindow.h"
#include "gui/snapperapi.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>

#include <KAboutData>
#include <KCrash>
#include <KDBusService>
#include <KMessageBox>
#include <KLocalizedString>

//#include <config.h>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("snaqqer")));

    KLocalizedString::setApplicationDomain("snaqqer");
    KAboutData aboutData (
        QStringLiteral("snaqqer"),
        xi18nc("@title", "<application>Snaqqer</application>"),
        QStringLiteral(VERSION),
        xi18nc("@title", "Graphical frontend to Snapper for BTRFS"),
        KAboutLicense::GPL_V3,
        xi18nc("@info:credit", "© 2019-2021 Henningstone"));
    aboutData.setOrganizationDomain(QByteArray("kde.org"));
    aboutData.setProductName(QByteArray("snaqqer"));

    //aboutData.addCredit(xi18nc("@info:credit", "Name"), i18nc("@info:credit", "Accomplishment"), QStringLiteral("e@ma.il"));

    aboutData.setHomepage(QStringLiteral("https://gitlab.com/Henningstone/snaqqer"));

    KAboutData::setApplicationData(aboutData);
    app.setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    KCrash::initialize();

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.addPositionalArgument(QStringLiteral("directory"), xi18nc("@info:shell", "Opens the PreviousVersions window for the specified location (optional)."), QStringLiteral("[directory...]"));

    parser.process(app);
    aboutData.processCommandLine(&parser);

    KDBusService service(KDBusService::Unique);

    // registerMetaTypes();
    snapper::SnapperApi::staticInit();

    //Config::instance(QStringLiteral("snaqqerrc"));

    const QStringList args = parser.positionalArguments();

    if(args.length() > 0)
    {
        OldversionWindow *mainWindow = new OldversionWindow(args.at(0));
        mainWindow->show();
        return app.exec();
    }
    else
    {
        MainWindow *mainWindow = new MainWindow();
        mainWindow->show();
        return app.exec();
    }
}
